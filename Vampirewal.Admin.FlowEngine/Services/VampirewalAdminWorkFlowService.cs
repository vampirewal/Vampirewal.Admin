﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    VampirewalAdminWorkFlowService
// 创建者：      杨程
// 创建日期：	    2023/1/13 12:55:43

//----------------------------------------------------------------*/

#endregion

using Vampirewal.Admin.FlowEngine.Interface;
using Vampirewal.Admin.FlowEngine.Models;
using Vampirewal.Core;
using Vampirewal.Core.Components;

namespace Vampirewal.Admin.FlowEngine.Services;

/// <summary>
///
/// </summary>
public partial class VampirewalAdminWorkFlowService : IVampirewalAdminWorkFlowService
{
    /// <summary>
    ///
    /// </summary>
    public VampirewalAdminWorkFlowService()
    {
        //构造函数
    }

    public bool FlowComplate()
    {
        throw new NotImplementedException();
    }

    public string StartFlow(string TenantId,int BillType)
    {
        var repFlowBase= GetSqlSugarRepository<FlowBaseInfo>();

        var flowbase= repFlowBase.Single(s => s.TenantId == TenantId && s.BillType == BillType && s.State == 1);

        throw new NotImplementedException();
    }

    /// <summary>
    /// 获取对应数据仓库
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    private SqlSugarRepository<T> GetSqlSugarRepository<T>() where T:class,new() 
    {
        return VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<T>>();
    }
}