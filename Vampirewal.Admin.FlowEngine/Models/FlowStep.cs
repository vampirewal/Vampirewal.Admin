﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    FlowStep
// 创建者：      杨程
// 创建日期：	    2023/1/13 14:30:12

//----------------------------------------------------------------*/
#endregion

using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vampirewal.Core.Models;

namespace Vampirewal.Admin.FlowEngine.Models;

/// <summary>
/// 流程节点关联的每一步模型
/// </summary>
[SugarTable("Flow_Step")]
public partial class FlowStep:DetailBaseModel
{
    /// <summary>
    /// 
    /// </summary>
    public FlowStep()
    {
        //构造函数
    }


    /// <summary>
    /// 父级ID
    /// </summary>
    [SugarColumn(IsNullable =true,ColumnDescription ="父级ID")]
    public string ParentId { get; set; }

    /// <summary>
    /// 节点类型
    /// </summary>
    [SugarColumn( ColumnDescription = "节点类型")]
    public int NodeType { get; set; }

    /// <summary>
    /// 节点名称
    /// </summary>
    [SugarColumn( ColumnDescription = "节点名称")]
    public string NodeName { get; set; }

    /// <summary>
    /// 节点编号
    /// </summary>
    [SugarColumn(ColumnDescription = "节点编号")]
    public string FlowNodeNo { get; set; }

    /// <summary>
    /// 上级节点ID
    /// </summary>
    [SugarColumn(IsNullable = true, ColumnDescription = "上级节点ID")]
    public string UpNodeId { get; set; }

    /// <summary>
    /// 下级节点ID
    /// </summary>
    [SugarColumn(IsNullable = true, ColumnDescription = "上级节点ID")]
    public string DownNodeId { get; set; }

    /// <summary>
    /// 驳回节点ID
    /// </summary>
    [SugarColumn(IsNullable = true, ColumnDescription = "上级节点ID")]
    public string RejectNodeId { get; set; }

    /// <summary>
    /// X坐标
    /// </summary>
    public double NodePositionX { get; set; }

    /// <summary>
    /// Y坐标
    /// </summary>
    public double NodePositionY { get; set; }
}
