﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    FlowBaseInfo
// 创建者：      杨程
// 创建日期：	    2023/1/17 12:57:42

//----------------------------------------------------------------*/

#endregion

using SqlSugar;
using Vampirewal.Admin.ShareModel.FlowEngine;
using Vampirewal.Core;
using Vampirewal.Core.Models;
using Vampirewal.Core.Models.TreeModel;

namespace Vampirewal.Admin.FlowEngine.Models;

/// <summary>
/// 流程基本信息
/// </summary>
[SugarTable("Flow_BaseInfo")]
public partial class FlowBaseInfo : BillBaseModel
{
    /// <summary>
    ///
    /// </summary>
    public FlowBaseInfo()
    {
        //构造函数
    }

    /// <summary>
    /// 流程编号
    /// </summary>
    public string FlowNo
    {
        get
        {
            return $"{OrgName}.{FlowName}.{BillId}";
        }
    }

    private string _FlowName;

    /// <summary>
    /// 流程名称
    /// </summary>
    public string FlowName
    { get => _FlowName; set { _FlowName = value; OnPropertyChanged(); OnPropertyChanged("FlowNo"); } }

    private string _FlowDescription;

    /// <summary>
    /// 流程描述
    /// </summary>
    public string FlowDescription
    { get => _FlowDescription; set { _FlowDescription = value; OnPropertyChanged(); } }

    private int _BillType;

    /// <summary>
    /// 关联单据类型
    /// </summary>
    public int BillType
    { get => _BillType; set { _BillType = value; OnPropertyChanged(); } }

    private string _OrgId;

    /// <summary>
    /// 机构ID
    /// </summary>
    public string OrgId
    { get => _OrgId; set { _OrgId = value; OnPropertyChanged(); } }

    private string _OrgName;

    /// <summary>
    /// 机构名称
    /// </summary>
    public string OrgName
    { get => _OrgName; set { _OrgName = value; OnPropertyChanged(); OnPropertyChanged("FlowNo"); } }

    private string _TenantId;

    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId
    { get => _TenantId; set { _TenantId = value; OnPropertyChanged(); } }

    private int _State;

    /// <summary>
    /// 流程状态
    /// </summary>
    public int State
    { get => _State; set { _State = value; OnPropertyChanged(); } }

    [SugarColumn(IsIgnore = true)]
    public string StateStr
    {
        get
        {
            return ((FlowState)State).GetDisplay();
        }
    }
}

public partial class FlowBaseInfo : ITreeNode
{
    public object GetCurrentNodeMark()
    {
        return BillId;
    }

    public object GetParentNodeMark()
    {
        return TenantId;
    }

    public bool IsParentNode(ITreeNode parentNode)
    {
        if (parentNode is FlowBaseInfo node)
            return TenantId == node.BillId;
        return false;
    }

    public bool IsSameNode(ITreeNode node)
    {
        if (node is FlowBaseInfo tmpNode)
            return BillId == tmpNode.BillId;
        return false;
    }

    public bool IsSameParent(ITreeNode node)
    {
        if (node is FlowBaseInfo tmpNode)
            return TenantId == tmpNode.TenantId;
        return false;
    }

    public bool IsSubNode(ITreeNode subNode)
    {
        if (subNode is FlowBaseInfo tmpNode)
            return tmpNode.TenantId == BillId;
        return false;
    }

    public void SetParentNodeMark(object parentObj)
    {
        TenantId = parentObj.ToString();
    }
}