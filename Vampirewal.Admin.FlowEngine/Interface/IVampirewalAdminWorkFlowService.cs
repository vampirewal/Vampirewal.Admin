﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    IVampirewalAdminWorkFlowService
// 创建者：      杨程
// 创建日期：	    2023/1/13 12:52:21

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.FlowEngine.Interface
{
    /// <summary>
    /// Vampirewal.Admin工作流服务
    /// </summary>
    public interface IVampirewalAdminWorkFlowService
    {
        /// <summary>
        /// 开启流程
        /// </summary>
        /// <returns>流程ID</returns>
        string StartFlow(string TenantId,int BillType);

        /// <summary>
        /// 流程结束
        /// </summary>
        /// <returns></returns>
        bool FlowComplate();
    }
}