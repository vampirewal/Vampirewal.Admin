﻿using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Vampirewal.Admin.Upgrade
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            DispatcherUnhandledException += GlobalExceptions;
        }

        /// <summary>
        /// 全局异常捕获(需重写)
        /// <para>错误信息：e.Exception.Message</para>
        /// <para>最后需要写e.Handled = true;证明异常已处理</para>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void GlobalExceptions(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            //IDialogMessage dialog = new VampirewalDialog();
            //dialog.ShowPopupWindow($"错误信息：{e.Exception.Message}",)
            //e.Handled = true;

            string LogDir = $@"{AppDomain.CurrentDomain.BaseDirectory}Log\UpdateLog";

            if (!Directory.Exists(LogDir))
            {
                Directory.CreateDirectory(LogDir);
            }

            string logFile = $@"{LogDir}\{DateTime.Now.ToString("yyyyMMdd")}.txt";

            File.AppendAllText(logFile, $"{e.Exception.ToString()}\r\n\r\n\r\n");
        }
    }
}
