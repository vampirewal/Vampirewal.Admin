﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    DicCategory
// 创建者：      杨程
// 创建日期：	    2022/12/15 13:35:18

//----------------------------------------------------------------*/
#endregion



namespace Vampirewal.Admin.Test;

/// <summary>
/// 字典类别
/// </summary>
public partial class Sys_DicCategory
{


    private string _DicCategoryName;
    /// <summary>
    /// 类别名称
    /// </summary>
    public string DicCategoryName
    {
        get
        {
            return _DicCategoryName;
        }
        set
        {
            _DicCategoryName = value;
        }
    }



    private bool _IsActive=true;
    /// <summary>
    /// 是否启用
    /// </summary>
    public bool IsActive
    {
        get
        {
            return _IsActive;
        }
        set
        {
            _IsActive = value;
        }
    }



    private bool _IsPublic=false;
    /// <summary>
    /// 是否公共使用
    /// </summary>
    public bool IsPublic
    {
        get
        {
            return _IsPublic;
        }
        set
        {
            _IsPublic = value;
        }
    }


    private string _TenantId;
    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId { get => _TenantId; set { _TenantId = value;  } }
}


