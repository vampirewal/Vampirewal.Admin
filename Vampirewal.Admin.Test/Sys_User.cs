﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Sys_User
// 创建者：      杨程
// 创建日期：	    2022/12/16 13:40:42

//----------------------------------------------------------------*/
#endregion

//using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Test;

/// <summary>
/// 人员
/// </summary>
public partial class Sys_User
{


    private string _BillId;
    /// <summary>
    /// 
    /// </summary>
    public string BillId
    {
        get
        {
            return _BillId;
        }
        set
        {
            _BillId = value;
        }
    }


    private string _UserId;
    /// <summary>
    /// 用户ID，可用于登陆
    /// </summary>
    public string UserId { get=> _UserId; set { _UserId = value;  } }

    private string _Password;
    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get => _Password; set { _Password = value;   } }

    private string _Name;
    /// <summary>
    /// 用户名
    /// </summary>
    public string Name { get => _Name; set { _Name = value;   } }

    private int _Gender;
    /// <summary>
    /// 性别(1是男，2是女)
    /// </summary>
    public int Gender { get => _Gender; set { _Gender = value;   } }

    //[JsonIgnore]
    public string GenderStr
    {
        get
        {
            if (Gender==1)
            {
                return "男";
            }
            return "女";
        }
    }

    private string _PhoneNum;
    /// <summary>
    /// 手机号码
    /// </summary>
    public string PhoneNum { get => _PhoneNum; set { _PhoneNum = value;   } }

    private string _Position;
    /// <summary>
    /// 职务信息
    /// </summary>
    public string Position { get => _Position; set { _Position = value;   } }

    private string _Email;
    /// <summary>
    /// 邮箱
    /// </summary>
    public string Email { get => _Email; set { _Email = value;   } }

    private string _DepartmentId;
    /// <summary>
    /// 部门ID
    /// </summary>
    public string DepartmentId { get => _DepartmentId; set { _DepartmentId = value;   } }
    

    private bool _IsCurrentDeptLeader;
    /// <summary>
    /// 是否当前部门负责人
    /// </summary>
    public bool IsCurrentDeptLeader { get => _IsCurrentDeptLeader; set { _IsCurrentDeptLeader = value;   } }


    private string _DirectLeaderId;
    /// <summary>
    /// 直属上级UserID
    /// </summary>
    public string DirectLeaderId { get => _DirectLeaderId; set { _DirectLeaderId = value;   } }

    private int _State=3;
    /// <summary>
    /// 激活状态: 1=已激活，2=已禁用，3=未激活
    /// </summary>
    public int State { get => _State; set { _State = value;   } }

    //[JsonIgnore]
    public string StateStr
    {
        get
        {
            switch (State)
            {
                case 1:
                    return "已激活";
                case 2:
                    return "已禁用";
                case 3:
                    return "未激活";
                default:
                    return "";
            }
        }
    }

    /// <summary>
    /// 全局唯一
    /// </summary>
    public string open_userid { get; set; }
    

    private string _OrgId;
    /// <summary>
    /// 机构ID
    /// </summary>
    public string OrgId
    {
        get { return _OrgId; }
        set { _OrgId = value;   }
    }

    private string _OrgName;
    /// <summary>
    /// 机构名称
    /// </summary>
    public string OrgName
    {
        get { return _OrgName; }
        set { _OrgName = value;   }
    }

    private string _TenantId;
    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId { get => _TenantId; set { _TenantId = value;   } }

    private bool _IsAdmin=false;
    /// <summary>
    /// 是否管理员
    /// </summary>
    public bool IsAdmin { get => _IsAdmin; set { _IsAdmin = value;   } }
}
