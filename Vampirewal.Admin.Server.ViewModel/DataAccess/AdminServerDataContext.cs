﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    AdminServerDataContext
// 创建者：      杨程
// 创建日期：	    2022/12/15 12:54:29

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.ViewModels.DataAccess;

/// <summary>
/// 数据上下文
/// </summary>
public partial class AdminServerDataContext : VampirewalCoreDataContext
{
    /// <summary>
    ///
    /// </summary>
    public AdminServerDataContext() : base()
    {
        //构造函数
        InitData();
    }

    protected override void CodeFirst()
    {
        #region [     日志表     ]

        CreateTable<Sys_OperationLog>();
        CreateTable<Sys_ErrorLog>();

        #endregion

        #region [     系统表     ]

        CreateTable<Sys_DicCategory>();
        CreateTable<Sys_Dic>();
        CreateTable<Sys_Menu>();
        CreateTable<Sys_Org>();
        CreateTable<Sys_User>();
        CreateTable<Sys_UserOrg>();
        CreateTable<Sys_RoleTemplate>();
        CreateTable<Sys_UserRole>();
        CreateTable<Sys_RolePermission>();
        CreateTable<Sys_Tenant>();
        CreateTable<Sys_AppVersion>();
        CreateTable<Sys_Attachment>();
        CreateTable<Sys_Notify>();

        #endregion

        #region [     流程引擎表     ]

        CreateTable<FlowBaseInfo>();
        CreateTable<FlowStep>();

        CreateTable<Flow>();
        CreateTable<Link>();
        CreateTable<Work_Task>();
        CreateTable<Unit>();
        CreateTable<Work>();

        #endregion
    }

    public override void DataExecuting(object OldValue, DataFilterModel entityInfo)
    {
        // 新增操作
        if (entityInfo.OperationType == DataFilterType.InsertByObject)
        {
            // 主键赋值
            if (entityInfo.EntityColumnInfo.IsPrimarykey && entityInfo.EntityColumnInfo.PropertyInfo.PropertyType == typeof(string))
            {
                if (entityInfo.PropertyName == "BillId" && ((dynamic)entityInfo.EntityValue).BillId == null)
                {
                    entityInfo.SetValue(Guid.NewGuid().ToString());
                }

                if (entityInfo.PropertyName == "DtlId" && ((dynamic)entityInfo.EntityValue).DtlId == null)
                {
                    entityInfo.SetValue(Guid.NewGuid().ToString());
                }
            }

            if (entityInfo.PropertyName == "CreateTime" && ((dynamic)entityInfo.EntityValue).CreateTime == null)
                entityInfo.SetValue(DateTime.Now);

            //if (VampirewalCoreContext.GetInstance().User != null)
            //{
            //    if (entityInfo.PropertyName == "CreateUserId")
            //    {
            //        var createUserId = ((dynamic)entityInfo.EntityValue).CreateUserId;
            //        if (createUserId == null || createUserId == 0)
            //            entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.BillId);
            //    }

            //    if (entityInfo.PropertyName == "CreateBy" && ((dynamic)entityInfo.EntityValue).CreateBy == null)
            //        entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.Name);
            //}
        }

        // 更新操作
        if (entityInfo.OperationType == DataFilterType.UpdateByObject)
        {
            if (entityInfo.PropertyName == "UpdateTime")
                entityInfo.SetValue(DateTime.Now);
            //if (entityInfo.PropertyName == "UpdateUserId" && ((dynamic)entityInfo.EntityValue).UpdateUserId == null)
            //    entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.BillId);
            //if (entityInfo.PropertyName == "UpdateBy" && ((dynamic)entityInfo.EntityValue).UpdateBy == null)
            //    entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.Name);
        }
    }

    /// <summary>
    /// 初始化数据
    /// </summary>
    private void InitData()
    {
        //判断机构有没有最顶级的系统机构
        if (!Client.Queryable<Sys_Org>().Any(a => a.OrgType == 0))
        {
            Sys_Org top = new Sys_Org()
            {
                OrgName = VampirewalCoreContext.GetInstance().GetOptions<AppBaseOptions>().AppChineseName,
                OrgType = 0,
                State = true,
                CreateBy = "administrator",
                ListId = Guid.NewGuid().ToString(),
                ParentId = Guid.Empty.ToString(),
                CreateUserId = Guid.Empty.ToString(),
                TenantId = Guid.Empty.ToString(),
            };

            Client.Insertable(top).ExecuteCommand();
        }
    }
}

public class AdminSqlSugarSetup : VampirewalCoreSqlSugarSetup
{
    public override void CodeFirst()
    {
        #region [     日志表     ]

        CreateTable<Sys_OperationLog>();
        CreateTable<Sys_ErrorLog>();

        #endregion

        #region [     系统表     ]

        CreateTable<Sys_DicCategory>();
        CreateTable<Sys_Dic>();
        CreateTable<Sys_Menu>();
        CreateTable<Sys_Org>();
        CreateTable<Sys_User>();
        CreateTable<Sys_UserOrg>();
        CreateTable<Sys_RoleTemplate>();
        CreateTable<Sys_UserRole>();
        CreateTable<Sys_RolePermission>();
        CreateTable<Sys_Tenant>();
        CreateTable<Sys_AppVersion>();
        CreateTable<Sys_Attachment>();
        CreateTable<Sys_Notify>();

        #endregion

        #region [     流程引擎表     ]

        CreateTable<FlowBaseInfo>();
        CreateTable<FlowStep>();

        CreateTable<Flow>();
        CreateTable<Link>();
        CreateTable<Work_Task>();
        CreateTable<Unit>();
        CreateTable<Work>();

        #endregion
    }

    public override void InitData()
    {
        var app= VampirewalCoreContext.GetInstance().GetContext<string>("AppBaseOptions").JsonToEntity<AppBaseOptions>();


        //判断机构有没有最顶级的系统机构
        if (!Client.Queryable<Sys_Org>().Any(a => a.OrgType == 0))
        {
            Sys_Org top = new Sys_Org()
            {
                OrgName = app.AppChineseName,
                OrgType = 0,
                State = true,
                CreateBy = "administrator",
                ListId = Guid.NewGuid().ToString(),
                ParentId = Guid.Empty.ToString(),
                CreateUserId = Guid.Empty.ToString(),
                TenantId = Guid.Empty.ToString(),
            };

            Client.Insertable(top).ExecuteCommand();
        }
    }

    public override void DataExecuting(object OldValue, DataFilterModel entityInfo)
    {
        //base.DataExecuting(OldValue, entityInfo);

        // 新增操作
        if (entityInfo.OperationType == DataFilterType.InsertByObject)
        {
            // 主键赋值
            if (entityInfo.EntityColumnInfo.IsPrimarykey && entityInfo.EntityColumnInfo.PropertyInfo.PropertyType == typeof(string))
            {
                if (entityInfo.PropertyName == "BillId" && ((dynamic)entityInfo.EntityValue).BillId == null)
                {
                    entityInfo.SetValue(Guid.NewGuid().ToString());
                }

                if (entityInfo.PropertyName == "DtlId" && ((dynamic)entityInfo.EntityValue).DtlId == null)
                {
                    entityInfo.SetValue(Guid.NewGuid().ToString());
                }
            }

            if (entityInfo.PropertyName == "CreateTime" && ((dynamic)entityInfo.EntityValue).CreateTime == null)
                entityInfo.SetValue(DateTime.Now);

            //if (VampirewalCoreContext.GetInstance().User != null)
            //{
            //    if (entityInfo.PropertyName == "CreateUserId")
            //    {
            //        var createUserId = ((dynamic)entityInfo.EntityValue).CreateUserId;
            //        if (createUserId == null || createUserId == 0)
            //            entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.BillId);
            //    }

            //    if (entityInfo.PropertyName == "CreateBy" && ((dynamic)entityInfo.EntityValue).CreateBy == null)
            //        entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.Name);
            //}
        }

        // 更新操作
        if (entityInfo.OperationType == DataFilterType.UpdateByObject)
        {
            if (entityInfo.PropertyName == "UpdateTime")
                entityInfo.SetValue(DateTime.Now);
            //if (entityInfo.PropertyName == "UpdateUserId" && ((dynamic)entityInfo.EntityValue).UpdateUserId == null)
            //    entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.BillId);
            //if (entityInfo.PropertyName == "UpdateBy" && ((dynamic)entityInfo.EntityValue).UpdateBy == null)
            //    entityInfo.SetValue(VampirewalCoreContext.GetInstance().User?.Name);
        }
    }
}