﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    LogJobService
// 创建者：      杨程
// 创建日期：	    2022/12/16 20:44:45

//----------------------------------------------------------------*/

#endregion



namespace Vampirewal.Admin.Server.ViewModels.JobService;

/// <summary>
/// 定时处理日志，每天0点执行一次，每次查出7天前的进行删除
/// </summary>
public class LogJobService : IJob
{

    private readonly SqlSugarRepository<Sys_OperationLog> _rep;

    /// <summary>
    ///
    /// </summary>
    public LogJobService(SqlSugarRepository<Sys_OperationLog> rep)
    {
        //构造函数
        _rep=rep;
    }

    public async Task ExcuteAsync(JobContext context)
    {
        //var rep = VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<Sys_OperationLog>>();

        var list = _rep.ToList(s => s.CreateTime <= DateTime.Now.AddDays(-7));

        if (list != null && list.Count > 0)
        {
            _rep.DeleteAsync(list.Select(s => s.BillId));
        }

        await Task.CompletedTask;
    }

    public async Task OnExcuteErrorAsync(JobContext context, Exception ex)
    {
        await Task.CompletedTask;
    }
}