﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    RpcLoginEvent
// 创建者：      杨程
// 创建日期：	    2023/1/5 22:18:41

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.ViewModels.EventBus;

/// <summary>
/// 针对RPC用户登陆之后的操作
/// </summary>
public partial class RpcLoginEvent : IVampirewalCoreEventFactory
{

    /// <summary>
    ///
    /// </summary>
    public RpcLoginEvent()
    {
        //构造函数
        
    }

    public string FactoryName => "RpcLoginEvent";

    /// <summary>
    /// 修改登陆用户的状态
    /// </summary>
    /// <param name="context"></param>
    [EventBusExcute]
    public void UpdateUserLoginState(EventContext context)
    {
        var repUser= VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<Sys_User>>();

        try
        {
            var entity = context.PassData as Sys_User;

            if (entity == null) return;

            repUser.CurrentBeginTran();

            repUser.Update(entity);

            repUser.CurrentCommitTran();
        }
        catch (Exception ex)
        {
            repUser.CurrentRollbackTran();
            throw ex;
        }
    }

    public void EventFactoryComplate(EventFactoryContext context)
    {
    }
}