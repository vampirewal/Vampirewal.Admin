﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    RemoveClientMenuEventBus
// 创建者：      杨程
// 创建日期：	    2023/2/5 18:03:15

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.ViewModels.EventBus;

/// <summary>
///移除用户菜单之后需要执行的事件
/// </summary>
public partial class RemoveClientMenuEventBus : IVampirewalCoreEventFactory
{
    /// <summary>
    ///
    /// </summary>
    public RemoveClientMenuEventBus()
    {
        //构造函数
    }

    public string FactoryName => "移除用户菜单之后需要执行的事件";

    public void EventFactoryComplate(EventFactoryContext context)
    {
    }

    /// <summary>
    /// 移除角色权限关联信息
    /// </summary>
    /// <param name="context"></param>
    //[SubscribeEvent("RemoveMenu")]
    [EventBusExcute]
    public void RemoveRolePermission(EventContext context)
    {
        var entity = context.PassData as Sys_Menu;

        if (entity == null)
            return;

        var repRolePermission = VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<Sys_RolePermission>>();

        try
        {
            repRolePermission.CurrentBeginTran();

            repRolePermission.Delete(d => d.PermissionId == entity.BillId);

            repRolePermission.CurrentCommitTran();
        }
        catch (Exception ex)
        {
            repRolePermission.CurrentRollbackTran();
        }
    }
}