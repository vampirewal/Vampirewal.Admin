﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    FlowEngineManagerViewModel
// 创建者：      杨程
// 创建日期：	    2022/12/17 23:00:19

//----------------------------------------------------------------*/

#endregion



namespace Vampirewal.Admin.Server.ViewModels.ViewModel.FlowEngineManager;

/// <summary>
///
/// </summary>
[VampirewalIoCRegister(ServerViewModelKeys.FlowEngineManagerViewModel)]
public partial class FlowEngineManagerViewModel : BillListBaseVM<FlowBaseInfo, FlowBaseInfo>
{
    /// <summary>
    ///
    /// </summary>
    public FlowEngineManagerViewModel(IVampirewalCoreDialogMessage dialog, SqlSugarRepository<FlowBaseInfo> _repFlow, SqlSugarRepository<FlowBaseInfo> _repSearch) : base(dialog, _repFlow, _repSearch)
    {
        //构造函数
        FlowList.LoadFullNodes(RepBill.ToList());
    }

    #region [     重写     ]

    protected override void InitVM()
    {
        FlowList = new TreeBuilderByFullLoad<FlowBaseInfo>();
    }

    protected override IUcViewBase SetView()
    {
        switch (ViewKey)
        {
            case ServerViewKeys.EditFlowEngineView:
                return WindowsManager.GetInstance().GetView(ViewKey);

            default:
                throw new NotImplementedException();
        }
    }

    #endregion

    #region [     属性     ]

    /// <summary>
    /// 底部页面
    /// </summary>
    [ObservableProperty]
    public IUcViewBase bottomContext;

    public TreeBuilderByFullLoad<FlowBaseInfo> FlowList { get; set; }

    public IVampirewalCoreEventCommand[] TreeListViewActionsCommand { get; set; }

    private string ViewKey;

    #endregion

    #region [     Command命令     ]

    [RelayCommand]
    public void LookDetail(FlowBaseInfo flow)
    {
        BottomContext = WindowsManager.GetInstance().GetView(ServerViewKeys.EditFlowEngineView, Param: flow);
    }

    [RelayCommand]
    public void TestAddNewFlow()
    {
        ViewKey = ServerViewKeys.EditFlowEngineView;

        SetDialogWindowSetting(new DialogWindowSetting()
        {
            WindowWidth = 800,
            WindowHeight = 600,
            IsShowMaxButton = false,
            IsShowMinButton = false,
            IsOpenWindowSize = false,
            TitleFontSize = 15,
            CloseDialogWindowCallBackCommand = new RelayCommand<object>(ExecuteBeforeCloseDialogWindow),
            IconStr = "pack://application:,,,/Vampirewal.Admin.Server;component/Logo.png"
        });

        ExecuteShowDialogWindow(null);
    }

    #endregion
}

[VampirewalIoCRegister(ServerViewModelKeys.EditFlowEngineViewModel)]
public partial class EditFlowEngineViewModel : BillVM<FlowBaseInfo>
{
    private IVampirewalCoreDialogMessage Dialog { get; set; }
    private SqlSugarRepository<FlowBaseInfo> repFlow { get; set; }

    public EditFlowEngineViewModel(IVampirewalCoreDialogMessage dialog, SqlSugarRepository<FlowBaseInfo> _repFlow) : base(_repFlow)
    {
        repFlow = _repFlow;
        Dialog = dialog;

        if (string.IsNullOrEmpty(Entity.BillId))
            Entity.BillId = Guid.NewGuid().ToString();

        _repFlow.Change<FlowStep>().ToList(g => g.BillId == Entity.BillId).ForEach(FlowSteps.Add);

        BillTypeEnum = EnumHelper.EnumToList<BillType>().Where(w => w.Classification >= 0 && w.Classification <= 100).OrderBy(w => w.Classification).ToList();
        FlowState = EnumHelper.EnumToList<FlowState>().Where(w => w.Classification >= 0 && w.Classification <= 1).OrderBy(w => w.Classification).ToList();
    }


    private List<EnumberCreditType> _BillTypeEnum;
    /// <summary>
    /// 
    /// </summary>
    public List<EnumberCreditType> BillTypeEnum
    {
        get
        {
            return _BillTypeEnum;
        }
        set
        {
            _BillTypeEnum = value;
            OnPropertyChanged();
        }
    }



    private List<EnumberCreditType> _FlowState;
    /// <summary>
    /// 
    /// </summary>
    public List<EnumberCreditType> FlowState
    {
        get
        {
            return _FlowState;
        }
        set
        {
            _FlowState = value;
            OnPropertyChanged();
        }
    }




    public override void PassData(object obj)
    {
        if (obj is FlowBaseInfo info)
        {
            SetEntity(info);

            Title = $"修改 {info.FlowNo} 流程";
        }
        else
        {
            Title = "新增流程";
        }
    }

    public override void InitData()
    {
        FlowSteps = new ObservableCollection<FlowStep>();
    }

    public ObservableCollection<FlowStep> FlowSteps { get; set; }

    [RelayCommand]
    public void SaveFlow(List<FlowStep> source)
    {
        source.ForEach(f => f.BillId = Entity.BillId);


    }
}