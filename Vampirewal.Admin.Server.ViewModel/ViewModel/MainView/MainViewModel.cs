﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    MainViewModel
// 创建者：      杨程
// 创建日期：	    2022/12/15 12:52:36

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.ViewModels.ViewModel.MainView;

/// <summary>
/// 主窗体VM
/// </summary>
[VampirewalIoCRegister(ServerViewModelKeys.MainViewModel)]
public partial class MainViewModel : ViewModelBase
{
    #region [     服务引用     ]



    private IVampirewalCoreDialogMessage Dialog { get; set; }

    private IService _TouchRpcService;

    public IService TouchRpcService
    { get => _TouchRpcService; set { _TouchRpcService = value; OnPropertyChanged(); } }

    //public string TouchRocStateStr
    //{
    //    get
    //    {
    //        string result = string.Empty;

    //        switch (TouchRpcService.ServerState)
    //        {
    //            case ServerState.None:
    //                result = "初建RPC";
    //                break;
    //            case ServerState.Running:
    //                result = "RPC运行中";
    //                break;
    //            case ServerState.Exception:
    //                result = "RPC遇到异常";
    //                break;
    //            case ServerState.Stopped:
    //                result = "RPC已停止";
    //                break;
    //            case ServerState.Disposed:
    //                result = "RPC已释放";
    //                break;

    //        }

    //        return result;
    //    }
    //}

    #endregion

    /// <summary>
    ///
    /// </summary>
    public MainViewModel(
                         IVampirewalCoreDialogMessage dialog,
                         
                         IService touchRpcService
                         ) : base()
    {
        
        Dialog = dialog;
        TouchRpcService = touchRpcService;
        //构造函数

        var config = VampirewalCoreContext.GetInstance().GetOptions<AppBaseOptions>();

        Title = $"{config.AppChineseName}(版本：{config.AppVersion})";

        //var aaa = VampirewalCoreContext.GetInstance().GetService<LoginService>();
    }

    #region 重写

    public override void InitData()
    {
        FunctionMenus = new ObservableCollection<FunctionMenu>();
    }

    #endregion

    #region 属性

    /// <summary>
    /// 主页面窗体内容
    /// </summary>
    [ObservableProperty]
    public IUcViewBase mainContext;

    public ObservableCollection<FunctionMenu> FunctionMenus { get; set; }

    #endregion

    #region Command命令

    /// <summary>
    /// 顶部模块切换命令
    /// </summary>
    /// <param name="ModuleStr"></param>
    [RelayCommand]
    public void SwichTopMenu(string ModuleStr)
    {
        FunctionMenus.Clear();

        switch (ModuleStr)
        {
            //系统管理
            case "SystemManager":
                {
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "菜单配置", ViewPath = ServerViewKeys.ClientMenuManagerView });
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "字典配置", ViewPath = ServerViewKeys.DicManagerView });
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "租户配置", ViewPath = ServerViewKeys.TenantManagerView });
                }
                break;

            case "OrganizationManager":
                {
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "机构部门", ViewPath = ServerViewKeys.OrgManagerView });
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "人员管理", ViewPath = ServerViewKeys.UserManagerView });
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "角色管理", ViewPath = ServerViewKeys.RoleManagerView });
                }
                break;

            case "FlowEngineManager":
                {
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "流程配置", ViewPath = ServerViewKeys.FlowEngineManagerView });
                }
                break;

            case "HomeManager":
                {
                    FunctionMenus.Add(new FunctionMenu() { MenuName = "当前在线", ViewPath = ServerViewKeys.OnlineUserView });
                }
                break;
        }
    }

    private string CurrentSelectMainView;

    /// <summary>
    /// 选择左侧菜单命令
    /// </summary>
    /// <param name="ViewKey"></param>
    [RelayCommand]
    public void SelectLeftMenu(string ViewKey)
    {
        if (string.IsNullOrEmpty(ViewKey))
        {
            MainContext = null;
            return;
        }

        if (ViewKey == CurrentSelectMainView)
        {
            //Dialog.ShowPopupWindow("当前窗体已经打开，请勿重复点击！", WindowsManager.GetInstance().MainWindow, MessageType.Error);
            Dialog.ShowPopupWindow(new Core.WindoSetting.PopupWindowSetting()
            {
                Message= "当前窗体已经打开，请勿重复点击！",
                OwnerWindow= WindowsManager.GetInstance().MainWindow,
                Type= Core.WindoSetting.MessageType.Error
            });
            return;
        }

        CurrentSelectMainView = ViewKey;
        MainContext = WindowsManager.GetInstance().GetView(ViewKey);
    }

    #endregion
}

[ObservableObject]
public partial class FunctionMenu
{
    /// <summary>
    /// 菜单名称
    /// </summary>
    [ObservableProperty]
    public string menuName;

    /// <summary>
    /// 窗体路径
    /// </summary>
    [ObservableProperty]
    public string viewPath;
}