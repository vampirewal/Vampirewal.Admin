﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    OnlineUserViewModel
// 创建者：      杨程
// 创建日期：	    2023/1/5 14:51:01

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.ViewModels.ViewModel.HomeManager;

/// <summary>
/// 当前在线人员VM
/// </summary>
[VampirewalIoCRegister(ServerViewModelKeys.OnlineUserViewModel)]
public partial class OnlineUserViewModel : BillListBaseVM<Sys_User, Sys_User>
{
    /// <summary>
    ///
    /// </summary>
    public OnlineUserViewModel(IVampirewalCoreDialogMessage dialog, SqlSugarRepository<Sys_User> _repUser, SqlSugarRepository<Sys_User> _repSearch) : base(dialog, _repUser, _repSearch)
    {
        //构造函数
    }

    #region [     重写     ]

    public override void MessengerRegister()
    {
    }

    protected override void InitVM()
    {
    }

    protected override IUcViewBase SetView()
    {
        throw new NotImplementedException();
    }

    #endregion

    #region [     Command命令     ]

    /// <summary>
    /// 查询在线用户
    /// </summary>
    [RelayCommand]
    public void QueryOnlineUser()
    {
        EntityList?.Clear();
        RepBill.ToList(s => s.IsOnline).ForEach(EntityList.Add);
    }

    #endregion
}