﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    OrgManagerViewModel
// 创建者：      杨程
// 创建日期：	    2022/12/16 20:17:52

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager;

/// <summary>
/// 机构管理VM
/// </summary>
[VampirewalIoCRegister(ServerViewModelKeys.OrgManagerViewModel)]
public partial class OrgManagerViewModel : BillListBaseVM<Sys_Org, Sys_Org>
{
    private SqlSugarRepository<Sys_Org> repOrg { get; set; }

    /// <summary>
    ///
    /// </summary>
    public OrgManagerViewModel(IVampirewalCoreDialogMessage dialog, SqlSugarRepository<Sys_Org> _repOrg, SqlSugarRepository<Sys_Org> _repSearch) : base(dialog, _repOrg, _repSearch)
    {
        repOrg = _repOrg;
        //构造函数

        OrgList.LoadFullNodes(_repOrg.ToList());

        TreeListViewActionsCommand = new IVampirewalCoreEventCommand[]
        {
            new VampirewalCoreEventCommand("SelectedItemChanged","SelectedItem", new RelayCommand<GeneralTreeContainer<Sys_Org>>(Selected =>
            {
                var menu = Selected.TreeNode;
            }))
        };
    }


    #region [     重写     ]

    protected override void InitVM()
    {
        OrgList = new TreeBuilderByFullLoad<Sys_Org>();
    }

    protected override IUcViewBase SetView()
    {
        switch (ViewKey)
        {
            case ServerViewKeys.EditClientMenuView:
                return WindowsManager.GetInstance().GetView(ViewKey);

            default:
                throw new NotImplementedException();
        }
    }

    public override void ExecuteBeforeCloseDialogWindow(object obj)
    {
        OrgList.LoadFullNodes(repOrg.ToList());
    }

    #endregion

    #region [     属性     ]

    /// <summary>
    /// 底部页面
    /// </summary>
    [ObservableProperty]
    public IUcViewBase bottomContext;

    public TreeBuilderByFullLoad<Sys_Org> OrgList { get; set; }

    public IVampirewalCoreEventCommand[] TreeListViewActionsCommand { get; set; }

    private string ViewKey;

    #endregion

    #region [     Command命令     ]

    [RelayCommand]
    public void LookDetail(Sys_Org org)
    {
        BottomContext = WindowsManager.GetInstance().GetView(ServerViewKeys.EditOrgView, Param: org);
    }

    #endregion
}

[VampirewalIoCRegister(ServerViewModelKeys.EditOrgViewModel)]
public partial class EditOrgViewModel : BillVM<Sys_Org>
{
    private SqlSugarRepository<Sys_Org> repOrg { get; set; }

    private IVampirewalCoreDialogMessage Dialog { get; set; }

    public EditOrgViewModel(IVampirewalCoreDialogMessage dialog, SqlSugarRepository<Sys_Org> _repOrg) : base(_repOrg)
    {
        repOrg = _repOrg;
        Dialog = dialog;
    }

    public override void PassData(object obj)
    {
        if (obj is Sys_Org org)
        {
            SetEntity(org);
        }
        else
        {
        }
    }

    [RelayCommand]
    public void Save()
    {
        if (!Entity.State && Entity.OrgType == 0)
        {
            Entity.State = true;
            //Dialog.ShowPopupWindow($"系统层级不能禁用！", WindowsManager.GetInstance().MainWindow, MessageType.Error);
            Dialog.ShowPopupWindow(new PopupWindowSetting()
            {
                Message= $"系统层级不能禁用！",
                OwnerWindow= WindowsManager.GetInstance().MainWindow,
                Type= MessageType.Error
            });
            return;
        }

        if (!Entity.State && Entity.OrgType == 1)
        {
            var Tenant = repOrg.Change<Sys_Tenant>().Single(s => s.BillId == Entity.TenantId);

            if (Tenant.EndTime > DateTime.Now)
            {
                Entity.State = true;
                //Dialog.ShowPopupWindow($"租户机构不能禁用！因为该租户还未到期！", WindowsManager.GetInstance().MainWindow, MessageType.Error);
                Dialog.ShowPopupWindow(new PopupWindowSetting()
                {
                    Message = $"租户机构不能禁用！因为该租户还未到期！",
                    OwnerWindow = WindowsManager.GetInstance().MainWindow,
                    Type = MessageType.Error
                });
                return;
            }
        }

        try
        {
            repOrg.CurrentBeginTran();

            repOrg.Update(Entity);

            repOrg.CurrentCommitTran();

            //Dialog.ShowPopupWindow($"保存成功！", WindowsManager.GetInstance().MainWindow, MessageType.Successful);
            Dialog.ShowPopupWindow(new PopupWindowSetting()
            {
                Message="保存成功!",
                OwnerWindow= WindowsManager.GetInstance().MainWindow,
                Type= MessageType.Successful
            });
        }
        catch (Exception ex)
        {
            repOrg.CurrentRollbackTran();
            //Dialog.ShowPopupWindow($"保存失败：{ex.Message}", WindowsManager.GetInstance().MainWindow, MessageType.Error);
            Dialog.ShowPopupWindow(new PopupWindowSetting()
            {
                Message = $"保存失败：{ex.Message}",
                OwnerWindow = WindowsManager.GetInstance().MainWindow,
                Type = MessageType.Error
            });
        }
    }
}