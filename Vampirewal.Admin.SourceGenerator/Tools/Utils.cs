﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Utils
// 创建者：      杨程
// 创建日期：	    2023/2/6 14:04:02

//----------------------------------------------------------------*/

#endregion

using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Vampirewal.Admin.SourceGenerator.Tools;

/// <summary>
/// 工具类
/// </summary>
public static class Utils
{
    #region 代码规范风格化

    /// <summary>
    /// 转换为Pascal风格-每一个单词的首字母大写
    /// </summary>
    /// <param name="fieldName">字段名</param>
    /// <param name="fieldDelimiter">分隔符</param>
    /// <returns></returns>
    public static string ConvertToPascal(string fieldName, string fieldDelimiter)
    {
        string result = string.Empty;
        if (fieldName.Contains(fieldDelimiter))
        {
            //全部小写
            string[] array = fieldName.ToLower().Split(fieldDelimiter.ToCharArray());
            foreach (var t in array)
            {
                //首字母大写
                result += t.Substring(0, 1).ToUpper() + t.Substring(1);
            }
        }
        else if (string.IsNullOrWhiteSpace(fieldName))
        {
            result = fieldName;
        }
        else if (fieldName.Length == 1)
        {
            result = fieldName.ToUpper();
        }
        else
        {
            result = fieldName.Substring(0, 1).ToUpper() + fieldName.Substring(1);
        }
        return result;
    }

    /// <summary>
    /// 转换为Camel风格-第一个单词小写，其后每个单词首字母大写
    /// </summary>
    /// <param name="fieldName">字段名</param>
    /// <param name="fieldDelimiter">分隔符</param>
    /// <returns></returns>
    public static string ConvertToCamel(string fieldName, string fieldDelimiter)
    {
        //先Pascal
        string result = ConvertToPascal(fieldName, fieldDelimiter);
        //然后首字母小写
        if (result.Length == 1)
        {
            result = result.ToLower();
        }
        else
        {
            result = result.Substring(0, 1).ToLower() + result.Substring(1);
        }

        return result;
    }

    #endregion

    /// <summary>
    /// 获取模版文件并转成string
    /// </summary>
    /// <param name="FileName">文件名称</param>
    /// <returns>string</returns>
    internal static string GetTemplateByFileName(string FileRelativePath)
    {
        Assembly assembly = Assembly.GetExecutingAssembly();

        string[] resNames = assembly.GetManifestResourceNames();
        using (Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.Template.{FileRelativePath}"))
        {
            if (stream != null)
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    string context = sr.ReadToEnd();

                    return context;
                }
            }
            else
            {
                return "";
            }
        }
    }

    internal static string GetContentInParentheses(string value)
    {
        var match = Regex.Match(value, @"\(([^)]*)\)");
        return match.Groups[1].Value;
    }
}