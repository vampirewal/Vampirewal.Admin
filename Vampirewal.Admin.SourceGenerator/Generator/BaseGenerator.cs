﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    BaseGenerator
// 创建者：      杨程
// 创建日期：	    2023/2/22 18:52:10

//----------------------------------------------------------------*/

#endregion

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using System.Collections.Generic;
using System.Text;
using Vampirewal.Admin.SourceGenerator.Tools;

namespace Vampirewal.Admin.SourceGenerator.Generator;

/// <summary>
///
/// </summary>
[Generator]
public partial class BaseCodeGenerator : ISourceGenerator
{
    public void Execute(GeneratorExecutionContext context)
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();
        attributes.Add("GeneratorDTOAttribute", Utils.GetTemplateByFileName("GeneratorDTO.txt"));
        attributes.Add("DtoIgnoreAttribute", Utils.GetTemplateByFileName("DtoIgnoreAttribute.txt"));

        foreach (var item in attributes)
        {
            context.AddSource(item.Key, SourceText.From(item.Value, Encoding.UTF8));
        }
    }

    public void Initialize(GeneratorInitializationContext context)
    {
    }
}