﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    FlowNode
// 创建者：      杨程
// 创建日期：	    2023/1/13 13:20:09

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using Vampirewal.Admin.FlowEngine.Models;

namespace Vampirewal.Admin.FlowEngineDesign;


/// <summary>
/// 流程节点
/// </summary>
public partial class FlowNode : Thumb
{
    private ResourceDictionary res
    {
        get
        {
            return new ResourceDictionary() { Source = new Uri("pack://application:,,,/Vampirewal.Admin.FlowEngineDesign;component/Design/FlowDesignStyles.xaml", UriKind.RelativeOrAbsolute) };
        }
    }

    static FlowNode()
    {
        DefaultStyleKeyProperty.OverrideMetadata(typeof(FlowNode), new FrameworkPropertyMetadata(typeof(FlowNode)));
    }


    /// <summary>
    /// 
    /// </summary>
    public FlowNode()
    {
        //构造函数

        var BaseStyle = res["Node"] as Style;

        this.Style = BaseStyle;

        

    }


    /// <summary>
    /// 归属流程的billid
    /// </summary>
    public string BillId { get; set; }

    private string _DtlId;
    /// <summary>
    /// 自身的ID
    /// </summary>
    public string DtlId 
    {
        get
        {
            

            return _DtlId;
        }
        set
        {
            _DtlId = value;

        }
    }

    /// <summary>
    /// 父级ID
    /// </summary>
    public string ParentId { get; set; }

    /// <summary>
    /// 存储线条（顶层节点不存这个）
    /// </summary>
    public System.Windows.Shapes.Path OwnPath { get; set; }

    #region [     依赖属性     ]

    #region 节点类型

    public int NodeType
    {
        get { return (int)GetValue(NodeTypeProperty); }
        set { SetValue(NodeTypeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for NodeType.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty NodeTypeProperty =
        DependencyProperty.Register("NodeType", typeof(int), typeof(FlowNode), new PropertyMetadata(0));

    #endregion

    #region 显示文字

    /// <summary>
    /// 节点名称
    /// </summary>
    public string NodeName
    {
        get { return (string)GetValue(NodeNameProperty); }
        set { SetValue(NodeNameProperty, value); }
    }

    // Using a DependencyProperty as the backing store for NodeName.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty NodeNameProperty =
        DependencyProperty.Register("NodeName", typeof(string), typeof(FlowNode), new PropertyMetadata("", NewNameCallBack));

    private static void NewNameCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        FlowNode node = (FlowNode)d;

        var name = e.NewValue.ToString();

        if (node.NodeType == 1)
        {
            node.NodeName = "开始";
        }

        else if (node.NodeType == 99)
        {
            node.NodeName = "结束";
        }
        else
        {
            if (string.IsNullOrEmpty(name))
            {
                node.NodeName = "执行中";
            }
        }
    }



    #endregion



    /// <summary>
    /// 节点编号
    /// </summary>
    public string FlowNodeNo
    {
        get { return (string)GetValue(FlowNodeNoProperty); }
        set { SetValue(FlowNodeNoProperty, value); }
    }

    // Using a DependencyProperty as the backing store for FlowNodeNo.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty FlowNodeNoProperty =
        DependencyProperty.Register("FlowNodeNo", typeof(string), typeof(FlowNode), new PropertyMetadata(""));



    /// <summary>
    /// 上级节点
    /// </summary>
    public FlowNode UpNode
    {
        get { return (FlowNode)GetValue(UpNodeProperty); }
        set { SetValue(UpNodeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for UpNode.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty UpNodeProperty =
        DependencyProperty.Register("UpNode", typeof(FlowNode), typeof(FlowNode), new PropertyMetadata(null));



    /// <summary>
    /// 下级节点
    /// </summary>
    public FlowNode DownNode
    {
        get { return (FlowNode)GetValue(DownNodeProperty); }
        set { SetValue(DownNodeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for DownNode.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty DownNodeProperty =
        DependencyProperty.Register("DownNode", typeof(FlowNode), typeof(FlowNode), new PropertyMetadata(null));



    /// <summary>
    /// 驳回节点
    /// </summary>
    public FlowNode RejectNode
    {
        get { return (FlowNode)GetValue(RejectNodeProperty); }
        set { SetValue(RejectNodeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for RejectNode.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty RejectNodeProperty =
        DependencyProperty.Register("RejectNode", typeof(FlowNode), typeof(FlowNode), new PropertyMetadata(null, RejectNodeCallBack));

    private static void RejectNodeCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        FlowNode flowNode = (FlowNode)d;
        
    }


    #region 坐标


    public double NodePositionX { get; set; }
    public double NodePositionY { get; set; }


    //public double NodePositionX
    //{
    //    get { return (double)GetValue(NodePositionXProperty); }
    //    set { SetValue(NodePositionXProperty, value); }
    //}

    //// Using a DependencyProperty as the backing store for NodePositionX.  This enables animation, styling, binding, etc...
    //public static readonly DependencyProperty NodePositionXProperty =
    //    DependencyProperty.Register("NodePositionX", typeof(double), typeof(FlowNode), new PropertyMetadata(1));



    //public double NodePositionY
    //{
    //    get { return (double)GetValue(NodePositionYProperty); }
    //    set { SetValue(NodePositionYProperty, value); }
    //}

    //// Using a DependencyProperty as the backing store for NodePositionY.  This enables animation, styling, binding, etc...
    //public static readonly DependencyProperty NodePositionYProperty =
    //    DependencyProperty.Register("NodePositionY", typeof(double), typeof(FlowNode), new PropertyMetadata(1));



    #endregion

    #endregion


}
