﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine
 * 唯一标识：c33dd75c-d5df-4263-a613-464a1716f3bc
 * 文件名：ApprovalFlowBase
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 15:52:57
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 流程配置基类
/// </summary>
public abstract class ApprovalFlowBase
{
    /// <summary>
    /// 流程管理器
    /// </summary>
    public ApprovalFlowManager _ApprovalFlowManager { get; set; }

    /// <summary>
    /// 流程编号
    /// </summary>
    public abstract string FlowCode { get; }

    /// <summary>
    /// 流程通知描述
    /// </summary>
    public abstract string WorkDescription { get; }

    /// <summary>
    /// 流程参数配置
    /// </summary>
    public List<string> FlowParams { get; set; }

    /// <summary>
    /// 节点集合
    /// </summary>
    public List<FlowNode> FlowNodes { get; set; }

    /// <summary>
    /// 添加流程配置节点
    /// </summary>
    public abstract void AddFlowNode();

    /// <summary>
    /// 节点连线集合
    /// </summary>
    public List<FlowLine> FlowLines { get; set; }

    /// <summary>
    /// 添加流程节点连线
    /// </summary>
    public abstract void AddFlowLine();

    /// <summary>
    /// 获取下一步节点集合
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public FlowNode GetNextNode(int index, List<FlowParam> @params)
    {
        FlowNode node = null;

        var currentLines = FlowLines.FindAll(f => f.PreviousNodeIndex == index);

        if (currentLines.Count == 1)
        {
            var currentNode = FlowNodes.FirstOrDefault(f => f.NodeIndex == currentLines[0].NextNodeIndex);

            if (currentNode != null)
            {
                node = currentNode;
            }
        }
        else if (currentLines.Count > 1)
        {
            foreach (var line in currentLines)
            {
                if (!string.IsNullOrEmpty(line.ConditionSelector))
                {
                    var condition = _ApprovalFlowManager.GetConditionSelector(line.ConditionSelector);

                    if (condition == null) continue;

                    foreach (var param in @params)
                    {
                        if (line.ConditionContent.Contains($"@{param.ParamName}"))
                            line.ConditionContent.Replace($"@{param.ParamName}", param.Value.ToString());
                    }

                    var result = condition.CanAccept(new ConditionInput()
                    {
                        Expression = line.ConditionContent,
                    });

                    if (result)
                    {
                        node = FlowNodes.FirstOrDefault(f => f.NodeIndex == line.NextNodeIndex);
                    }
                }
                else
                {
                    //throw Oops.Bah($"FlowCode:{FlowCode},lineId:{line.LineId}。\r\n异常信息：多条件分支需配置执行条件！");
                }
            }
        }

        return node;
    }
}