﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine.Attributes
 * 唯一标识：027e8647-3572-4b52-a3f8-b00f474e1c34
 * 文件名：FlowParamAttribute
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 16:25:45
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 流程参数特性
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class FlowParamAttribute : Attribute
{
    public FlowParamAttribute(string paramName)
    {
        ParamName = paramName;
    }

    /// <summary>
    /// 参数名称
    /// </summary>
    public string ParamName { get; set; }
}