﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine.Attributes
 * 唯一标识：135fb753-17d3-4624-987f-064813a8dc3a
 * 文件名：ConditionSelectorAttribute
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 15:17:47
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 条件选择器特性
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public partial class ConditionSelectorAttribute : Attribute
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="key">key值</param>
    /// <param name="description">描述</param>
    public ConditionSelectorAttribute(string key, string description)
    {
        Key = key;
        Description = description;
    }

    /// <summary>
    /// Key值
    /// </summary>
    public string Key { get; set; }

    /// <summary>
    /// 描述
    /// </summary>
    public string Description { get; set; }
}