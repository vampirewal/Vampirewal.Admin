﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine.Models
 * 唯一标识：8652102c-2f32-45db-aaa6-dcb83d125344
 * 文件名：FlowParam
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 16:10:20
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 流程参数
/// </summary>
public sealed class FlowParam
{
    /// <summary>
    /// 参数名称
    /// </summary>
    public string ParamName { get; set; }

    /// <summary>
    /// 参数值
    /// </summary>
    public object Value { get; set; }
}