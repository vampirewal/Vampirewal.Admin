﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine.Models
 * 唯一标识：9d7a213c-1ace-4c63-810a-3e816a25b4b7
 * 文件名：FlowNode
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 16:09:03
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 流程节点
/// </summary>
public sealed class FlowNode
{
    /// <summary>
    /// 节点ID
    /// </summary>
    public string NodeId { get; set; }

    /// <summary>
    /// 节点序号
    /// </summary>
    public int NodeIndex { get; set; }

    /// <summary>
    /// 节点名称
    /// </summary>
    public string NodeName { get; set; }

    /// <summary>
    /// 节点类型
    /// </summary>
    public NodeType Type { get; set; }

    /// <summary>
    /// 用户选择器
    /// </summary>
    public string UserSelector { get; set; }

    /// <summary>
    /// 驳回节点序号
    /// </summary>
    public int? RejectNodeIndex { get; set; }

    /// <summary>
    /// 用户选择器默认参数
    /// </summary>
    public UserSelectorInput DefaultUserSelectorInput { get; set; }
}

/// <summary>
/// 流程节点连线
/// </summary>
public sealed class FlowLine
{
    /// <summary>
    /// 连线ID
    /// </summary>
    public string LineId { get; set; }

    /// <summary>
    /// 连线名称
    /// </summary>
    public string LineName { get; set; }

    /// <summary>
    /// 描述
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// 上一个节点序号
    /// </summary>
    public int PreviousNodeIndex { get; set; }

    /// <summary>
    /// 下一个节点序号
    /// </summary>
    public int NextNodeIndex { get; set; }

    /// <summary>
    /// 条件选择器名称
    /// </summary>
    public string ConditionSelector { get; set; }

    /// <summary>
    /// 条件内容
    /// </summary>
    public string ConditionContent { get; set; }
}

/// <summary>
/// 节点类型
/// </summary>
public enum NodeType
{
    /// <summary>
    /// 开始节点
    /// </summary>
    [Description("开始节点")]
    Start = 0,

    /// <summary>
    /// 普通节点
    /// </summary>
    [Description("普通节点")]
    Normal = 1,

    /// <summary>
    /// 会签节点
    /// </summary>
    [Description("会签节点")]
    Countersign = 2,

    /// <summary>
    /// 或签
    /// </summary>
    [Description("或签")]
    Endorsement = 3,

    /// <summary>
    /// 结束节点
    /// </summary>
    [Description("结束节点")]
    End = 99,
}