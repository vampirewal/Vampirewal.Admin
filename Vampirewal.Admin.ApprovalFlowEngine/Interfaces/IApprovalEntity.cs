﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine
 * 唯一标识：4aac9f44-6371-4027-891c-c807e54537cd
 * 文件名：IApprovalEntity
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 14:30:08
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 审批模型
/// </summary>
public interface IApprovalEntity<TWorkId>
{
    /// <summary>
    /// 流程ID
    /// </summary>
    TWorkId WorkId { get; set; }

    /// <summary>
    /// 审核流程状态
    /// </summary>
    WorkState AuditState { get; set; }
}

/// <summary>
/// 流程状态
/// </summary>
public enum WorkState
{
    /// <summary>
    /// 取消
    /// </summary>
    [Display(Name = "取消")]
    Cancel = -1,

    /// <summary>
    /// 运行中
    /// </summary>
    [Display(Name = "运行中")]
    Running = 1,

    /// <summary>
    /// 完成
    /// </summary>
    [Display(Name = "完成")]
    Complate = 2,
}