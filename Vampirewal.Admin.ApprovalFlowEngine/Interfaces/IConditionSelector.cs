﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine.Interfaces
 * 唯一标识：83820ad9-e8c3-4c60-9035-3d308fa8939f
 * 文件名：IConditionSelector
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 15:16:09
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 条件选择器
/// </summary>
internal interface IConditionSelector
{
    /// <summary>
    /// 是否通过
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    bool CanAccept(ConditionInput input);
}

internal class ConditionInput
{
    /// <summary>
    /// 条件
    /// </summary>
    public string Expression { get; internal set; }

    /// <summary>
    /// 传输的数据
    /// </summary>
    public string PassData { get; set; }
}