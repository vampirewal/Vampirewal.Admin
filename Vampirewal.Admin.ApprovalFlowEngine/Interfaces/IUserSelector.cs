﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine.Interfaces
 * 唯一标识：2634ae43-de03-4ebc-bd0b-07a9bf4c2dd0
 * 文件名：IUserSelector
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 15:10:24
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

using Vampirewal.Admin.ShareModel.Models.Systems.User;

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 节点用户选择器
/// </summary>
public interface IUserSelector
{
    /// <summary>
    /// 获取需要审核的用户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<Sys_User>> GetTargetUsersAsync(UserSelectorInput input);
}

public sealed class UserSelectorInput
{
    /// <summary>
    /// 机构ID
    /// </summary>
    public string OrgId { get; set; }

    /// <summary>
    /// 用户ID
    /// </summary>
    public string UserId { get; set; }

    /// <summary>
    /// 岗位名称
    /// </summary>
    public string Position { get; set; }

    /// <summary>
    /// 角色ID
    /// </summary>
    public string RoleId { get; set; }

}