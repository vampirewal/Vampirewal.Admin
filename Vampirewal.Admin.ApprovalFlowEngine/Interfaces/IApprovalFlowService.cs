﻿#region << 文件说明 >>

/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ApprovalFlowEngine.Interfaces
 * 唯一标识：0a5ead95-a4fb-4815-9fd1-3be15810a2df
 * 文件名：IApprovalFlowService
 *
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/14 17:03:18
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion << 文件说明 >>

namespace Vampirewal.Admin.ApprovalFlowEngine;

/// <summary>
/// 审批流程服务
/// </summary>
public interface IApprovalFlowService
{
    /// <summary>
    /// 开始流程
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<object> StartFlow(StartFlowInput input);
}

public class ApprovalFlowService : IApprovalFlowService
{
    private readonly ApprovalFlowManager Manager;

    public ApprovalFlowService(ApprovalFlowManager manager)
    {
        Manager = manager;
    }

    public async Task<object> StartFlow(StartFlowInput input)
    {
        var flow = Manager.GetWorkFlow(input.FlowCode);

        if (flow == null)
            throw new Exception($"未找到FlowCode为{input.FlowCode}的审批流程！");

        foreach (var item in input.FlowParams)
        {
            if (flow.WorkDescription.Contains($"@{item.ParamName}"))
                flow.WorkDescription.Replace($"@{item.ParamName}", item.Value.ToString());
        }

        //这里的返回值是随便写个免得报错的
        return Task.FromResult(flow);
    }
}

public class StartFlowInput
{
    /// <summary>
    /// 流程类型
    /// </summary>
    public string FlowCode { get; set; }

    /// <summary>
    /// 单据主键ID
    /// </summary>
    public object BillId { get; set; }

    /// <summary>
    /// 单据编号
    /// </summary>
    public string BillNo { get; set; }

    /// <summary>
    /// 传输过来的参数值
    /// </summary>
    public List<FlowParam> FlowParams { get; set; }
}