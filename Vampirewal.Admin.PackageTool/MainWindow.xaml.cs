﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vampirewal.Core.Attributes;
using Vampirewal.Core.WPF.Theme;

namespace Vampirewal.Admin.PackageTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [VampirewalIoCRegister("Vampirewal.Admin.PackageTool.MainWindow")]
    public partial class MainWindow : MainWindowBase
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public override string ViewModelKey => "Vampirewal.Admin.PackageTool.MainViewModel";
    }
}
