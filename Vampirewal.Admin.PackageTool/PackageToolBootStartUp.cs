﻿#region [     文件信息     ]

/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.PackageTool
 * 唯一标识：19ad9e02-85f8-4681-b621-0c7d86834570
 * 文件名：PackageToolBootStartUp
 * 当前用户域：VAMPIREWAL
 *
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/16 12:52:52
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion [     文件信息     ]

using Microsoft.Extensions.DependencyInjection;
using System;
using Vampirewal.Admin.ShareCommon;
using Vampirewal.Core.Components;
using Vampirewal.Core.Extensions;
using Vampirewal.Core.Interface;

namespace Vampirewal.Admin.PackageTool;

/// <summary>
///
/// </summary>
public partial class PackageToolBootStartUp : VampirewalBootStartUp
{
    protected override string FirstViewKey => "Vampirewal.Admin.PackageTool.MainWindow";

    public override void OnAppExit(bool IsRun)
    {

    }

    public override void OnAppRun(bool IsRun)
    {

    }

    protected override void RegisterService(IServiceCollection services)
    {
        services.AddVampirewalCoreConfig($"{AppDomain.CurrentDomain.BaseDirectory}AppConfig.json", options =>
        {
            options.RegisterOptions<ServerOptions>();
            options.RegisterOptions<PackageOptions>();
        });

        //services.AddSingleton<IVampirewalCoreDialogMessage, VampirewalCoreDialogService>();
        services.AddVampirewalCoreDialogMessage();
    }
}