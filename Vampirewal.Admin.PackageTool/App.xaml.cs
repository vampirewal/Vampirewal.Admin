﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Vampirewal.Core.Components;
using Vampirewal.Core.WPF.Theme;

namespace Vampirewal.Admin.PackageTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : VampirewalApplication
    {
        protected override Type BootStartUp()
        {
            return typeof(PackageToolBootStartUp);
        }


    }
}
