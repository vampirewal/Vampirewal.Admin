﻿#region [     文件信息     ]
/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.PackageTool
 * 唯一标识：d75bf2c9-b725-428f-a1ac-947005bc53f0
 * 文件名：FilePlugin
 * 当前用户域：VAMPIREWAL
 * 
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/17 16:53:50
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion [     文件信息     ]

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TouchSocket.Rpc.TouchRpc;

namespace Vampirewal.Admin.PackageTool
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FilePlugin : TouchRpcPluginBase<HttpTouchRpcClient>
    {
        protected override void OnFileTransfering(HttpTouchRpcClient client, FileOperationEventArgs e)
        {
            e.IsPermitOperation = true;
            base.OnFileTransfering(client, e);
        }

    }
}
