﻿namespace Vampirewal.Admin.Client;

/// <summary>
/// MainView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ClientViewKeys.MainView)]
public partial class MainView : MainWindowBase
{
    public MainView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ClientViewModelKeys.MainViewModel;
}