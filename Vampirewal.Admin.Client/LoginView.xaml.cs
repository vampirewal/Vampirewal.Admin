﻿namespace Vampirewal.Admin.Client;

/// <summary>
/// LoginView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ClientViewKeys.LoginView)]
public partial class LoginView : WindowBase
{
    public LoginView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ClientViewModelKeys.LoginViewModel;

}