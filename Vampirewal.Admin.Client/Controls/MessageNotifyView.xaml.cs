﻿

namespace Vampirewal.Admin.Client.Controls;

/// <summary>
/// MessageNotifyView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ClientViewKeys.MessageNotifyView)]
public partial class MessageNotifyView : AddOrEditUcViewBase
{
    public MessageNotifyView()
    {
        InitializeComponent();
    }
}