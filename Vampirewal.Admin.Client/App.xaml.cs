﻿using System.Windows.Threading;
using Vampirewal.Admin.Client.ViewModels;
using Vampirewal.Core;
using Vampirewal.Core.Components;
using Vampirewal.Core.Interface;
using Vampirewal.Core.SimpleMVVM;
using Vampirewal.Core.WPF.Theme;

namespace Vampirewal.Admin.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : VampirewalApplication
    {
        private IVampirewalCoreDialogMessage Dialog { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Dialog=VampirewalCoreContext.GetInstance().GetService<IVampirewalCoreDialogMessage>();
        }

        protected override Type BootStartUp()
        {
            return typeof(ClientBootStartUp);
        }

        protected override void GlobalExceptions(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Dialog.ShowPopupWindow(new Core.WindoSetting.PopupWindowSetting()
            {
                IsKeepOpen=true,
                Message= e.Exception.ToString(),
                OwnerWindow= WindowsManager.GetInstance().MainWindow,
                Type= Core.WindoSetting.MessageType.Error
            });

            e.Handled = true;
        }
    }
}