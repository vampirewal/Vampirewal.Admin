﻿namespace Vampirewal.Admin.Client.Views;

/// <summary>
/// GeneralListView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ClientViewKeys.GeneralListView)]
public partial class GeneralListView : LayoutUcViewBase
{
    public GeneralListView()
    {
        InitializeComponent();
    }
}