﻿

namespace Vampirewal.Admin.Client.SystemViews.UserManager;

/// <summary>
/// EditSysUserView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ClientViewKeys.EditSysUserView)]
public partial class EditSysUserView : AddOrEditUcViewBase
{
    public EditSysUserView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ClientViewModelKeys.EditSysUserViewModel;
}
