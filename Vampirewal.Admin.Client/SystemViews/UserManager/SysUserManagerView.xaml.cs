﻿namespace Vampirewal.Admin.Client.SystemViews.UserManager;

/// <summary>
/// SysUserManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ClientViewKeys.SysUserManagerView)]
public partial class SysUserManagerView : LayoutUcViewBase
{
    public SysUserManagerView()
    {
        InitializeComponent();
    }

    //public override string ViewModelKey => ViewModelKeys.SysUserManagerViewModel;
}