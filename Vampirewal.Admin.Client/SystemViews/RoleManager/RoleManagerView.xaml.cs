﻿namespace Vampirewal.Admin.Client.SystemViews.RoleManager;

/// <summary>
/// RoleManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ClientViewKeys.RoleManagerView)]
public partial class RoleManagerView : LayoutUcViewBase
{
    public RoleManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ClientViewModelKeys.RoleManagerViewModel;
}