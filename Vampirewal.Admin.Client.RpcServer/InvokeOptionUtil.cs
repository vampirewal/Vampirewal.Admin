﻿#region [     文件信息     ]

/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.Client.ClientRpcServer
 * 唯一标识：7fc84d2e-b42c-41f2-9670-d000d7ef6bd0
 * 文件名：InvokeOptionUtil
 * 当前用户域：VAMPIREWAL
 *
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/2/27 18:19:07
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion [     文件信息     ]

using TouchSocket.Rpc;
using TouchSocket.Rpc.TouchRpc;

namespace Vampirewal.Admin.Client.ClientRpcServer;

/// <summary>
/// InvokeOption工具类
/// </summary>
public static class InvokeOptionUtil
{
    public static InvokeOption Fast_WaitInvoke_10s = new InvokeOption()
    {
        FeedbackType = FeedbackType.WaitInvoke,
        SerializationType = TouchSocket.Core.SerializationType.FastBinary,
        Timeout = 10 * 1000
    };

    public static InvokeOption Fast_WaitInvoke_30s = new InvokeOption()
    {
        FeedbackType = FeedbackType.WaitInvoke,
        SerializationType = TouchSocket.Core.SerializationType.FastBinary,
        Timeout = 30 * 1000
    };

    public static InvokeOption Fast_WaitInvoke_60s = new InvokeOption()
    {
        FeedbackType = FeedbackType.WaitInvoke,
        SerializationType = TouchSocket.Core.SerializationType.FastBinary,
        Timeout = 60 * 1000
    };
}