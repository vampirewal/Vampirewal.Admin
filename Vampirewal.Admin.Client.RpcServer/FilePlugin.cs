﻿#region [     文件信息     ]

/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.Client.ClientRpcServer
 * 唯一标识：941c8151-824a-4e85-9961-fb839d014830
 * 文件名：FilePlugin
 * 当前用户域：VAMPIREWAL
 *
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/19 21:02:13
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion [     文件信息     ]

using TouchSocket.Rpc.TouchRpc;

namespace Vampirewal.Admin.Client.ClientRpcServer;

/// <summary>
///
/// </summary>
public partial class FilePlugin : TouchRpcPluginBase<HttpTouchRpcClient>
{
    protected override void OnFileTransfering(HttpTouchRpcClient client, FileOperationEventArgs e)
    {
        e.IsPermitOperation = true;
        base.OnFileTransfering(client, e);
    }
}