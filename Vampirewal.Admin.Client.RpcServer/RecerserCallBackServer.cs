﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    RecerserCallBackServer
// 创建者：      杨程
// 创建日期：	    2023/1/18 12:34:11

//----------------------------------------------------------------*/

#endregion

using TouchSocket.Rpc;
using TouchSocket.Rpc.TouchRpc;
using Vampirewal.Core.Components;

namespace Vampirewal.Admin.Client.ClientRpcServer;

/// <summary>
/// 客户端接收消息回调RPC服务
/// </summary>
public partial class ReverseCallbackServer : RpcServer
{
    /// <summary>
    ///
    /// </summary>
    public ReverseCallbackServer()
    {
        //构造函数
    }

    /// <summary>
    /// 接收通知消息
    /// </summary>
    /// <param name="Msg"></param>
    [TouchRpc]
    public void GetNotifyMessage(string Msg)
    {
        VampirewalMessenger.GetInstance().Send("ShowNotifyWindow", Msg);
    }
}