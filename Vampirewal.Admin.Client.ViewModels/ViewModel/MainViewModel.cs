﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    MainViewModel
// 创建者：      杨程
// 创建日期：	    2022/12/20 21:13:35

//----------------------------------------------------------------*/

#endregion

using Vampirewal.Admin.ShareCommon;
using Vampirewal.Core.WindoSetting;

namespace Vampirewal.Admin.Client.ViewModels.ViewModel;

/// <summary>
/// 主窗体VM
/// </summary>
[VampirewalIoCRegister(ClientViewModelKeys.MainViewModel)]
public partial class MainViewModel : ViewModelBase
{
    private IRpcService RpcService { get; set; }

    private IVampirewalCoreDialogMessage Dialog { get; set; }

    private IApplicationService ApplicationService { get; set; }

    /// <summary>
    ///
    /// </summary>
    public MainViewModel(IRpcService rpcService, IVampirewalCoreDialogMessage dialog, IApplicationService application) : base()
    {
        //构造函数
        RpcService = rpcService;
        Dialog = dialog;
        ApplicationService= application;
        //构造函数

        Title = VampirewalCoreContext.GetInstance().GetOptions<AppBaseOptions>().AppChineseName;

        SysMenus = VampirewalCoreContext.GetInstance().GetContext<List<Sys_MenuVO>>("ClientMenu");

        SysMenus.FindAll(f => f.MenuType == 0).ForEach(TopModuleMenus.Add);
    }

    #region [     重写     ]

    public override void InitData()
    {
        TopModuleMenus = new ObservableCollection<Sys_MenuVO>();
        LeftMenus = new ObservableCollection<Sys_MenuVO>();
    }

    public override void CloseWindow()
    {
        //这里控制是直接关闭整个系统，还是返回到登陆界面
        

        RpcService.CallApi<bool>(RpcMethod.LoginOut, InvokeOptionUtil.Fast_WaitInvoke_10s, response =>
        {
            WindowResult = null;
        }, ex =>
        {
            WindowResult = false;
        });
    }

    private bool? WindowResult = false;

    public override object GetResult()
    {
        VampirewalMessenger.GetInstance().Unregister(this);

        return WindowResult;
    }

    public override void MessengerRegister()
    {
        VampirewalMessenger.GetInstance().Register<string>(this, "ShowNotifyWindow", ShowNotifyWindow);
    }

    #endregion

    #region [     属性     ]

    /// <summary>
    /// 主窗体显示内容
    /// </summary>
    [ObservableProperty]
    public IUcViewBase mainContext;

    /// <summary>
    /// 当前显示的ViewPath路径，用于下方判断，避免重复点击
    /// </summary>
    private string CurrentShowViewPath;

    /// <summary>
    /// 获取到的全部菜单
    /// </summary>
    public List<Sys_MenuVO> SysMenus { get; set; }

    /// <summary>
    /// 顶部模块
    /// </summary>
    public ObservableCollection<Sys_MenuVO> TopModuleMenus { get; set; }

    /// <summary>
    /// 左侧菜单
    /// </summary>
    public ObservableCollection<Sys_MenuVO> LeftMenus { get; set; }

    #endregion

    #region [     私有方法     ]

    /// <summary>
    /// 接收消息并弹窗
    /// </summary>
    /// <param name="Msg"></param>
    private void ShowNotifyWindow(string Msg)
    {
        if (string.IsNullOrEmpty(Msg))
            return;

        try
        {
            //存在跨UI线程调用的情况，写个trycatch包裹看看
            ApplicationService.BeginInvoke((() =>
            {
                Dialog.OpenNotifyWindow(new NotifyWindowSetting()
                {
                    UcView = WindowsManager.GetInstance().GetView(ClientViewKeys.MessageNotifyView, ClientViewModelKeys.MessageNotifyViewModel),
                    NotifyTitle = $"通知信息",
                    PassData = Msg,
                    WindowHeight = 600,
                    DelayCloseTime = 5
                });
            }));
        }
        catch (Exception ex)
        {
        }
    }

    #endregion

    #region [     Command命令     ]

    /// <summary>
    /// 顶部模块切换命令
    /// </summary>
    /// <param name="ModuleListId"></param>
    [RelayCommand]
    public void ModuleChange(string ModuleListId)
    {
        LeftMenus?.Clear();

        SysMenus.FindAll(f => f.ParentId == ModuleListId && f.MenuType == 1).ForEach(LeftMenus.Add);

        //#region 模拟推送弹窗

        //Sys_NotifyVO vo = new Sys_NotifyVO()
        //{
        //    BillId=Guid.NewGuid().ToString(),
        //    SendUserId= VampirewalCoreContext.GetInstance().GetContext<string>("BillId"),
        //    CreateBy= VampirewalCoreContext.GetInstance().GetContext<string>("Name"),
        //    CreateTime= DateTime.Now,
        //    CreateUserId= VampirewalCoreContext.GetInstance().GetContext<string>("BillId"),
        //    IsReceived=false,
        //    NotifyMsg="hahahahaa",
        //    NotifyType=3,
        //    OrgId= VampirewalCoreContext.GetInstance().GetContext<string>("OrgId"),
        //    //ReceivedTime=DateTime.Now,
        //    TenantId= VampirewalCoreContext.GetInstance().GetContext<string>("TenantId"),
        //    ReceiveUserId= VampirewalCoreContext.GetInstance().GetContext<string>("BillId"),
        //};

        //RpcService.CallApi(RpcMethod.SendMessage, InvokeOption.OnlySend, (ex) =>
        //{
        //}, vo);
        //#endregion
    }

    /// <summary>
    /// 选择左侧菜单命令
    /// </summary>
    /// <param name="vo"></param>
    [RelayCommand]
    public void SelectLeftMenu(Sys_MenuVO vo)
    {
        if (!string.IsNullOrEmpty(CurrentShowViewPath) && CurrentShowViewPath.Equals(vo.ViewPath))
        {
            Dialog.ShowPopupWindow(new PopupWindowSetting()
            {
                Message = "当前页面已打开，请勿重复点击！",
                OwnerWindow = WindowsManager.GetInstance().MainWindow,
                Type = MessageType.Error
            });

            return;
        }

        try
        {
            MainContext = WindowsManager.GetInstance().GetView(vo.ViewPath, vo.ViewModelPath);
            CurrentShowViewPath = vo.ViewPath;
        }
        catch (Exception ex)
        {
            //Dialog.ShowPopupWindow(ex.Message, WindowsManager.GetInstance().MainWindow, MessageType.Error);
            Dialog.ShowPopupWindow(new PopupWindowSetting()
            {
                Message = ex.Message,
                OwnerWindow = WindowsManager.GetInstance().MainWindow,
                Type = MessageType.Error
            });
        }
    }

    #endregion
}