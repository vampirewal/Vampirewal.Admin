﻿#region [     文件信息     ]
/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.Client.ViewModels.ViewModel.SysManager
 * 唯一标识：187c8426-be62-4368-95c1-4217406211a8
 * 文件名：SysRoleManagerViewModel
 * 当前用户域：VAMPIREWAL
 * 
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/3 13:50:38
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion [     文件信息     ]

using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vampirewal.Core.GeneralTree;

namespace Vampirewal.Admin.Client.ViewModels.ViewModel.SysManager;

/// <summary>
/// 
/// </summary>
[VampirewalIoCRegister(ClientViewModelKeys.RoleManagerViewModel)]
public partial class SysRoleManagerViewModel : BillListBaseVM<Sys_RoleTemplateVO, Sys_RoleTemplateVO>
{
    private IRpcService RpcService { get; set; }
    public SysRoleManagerViewModel(IVampirewalCoreDialogMessage dialog, IRpcService rpcService) : base()
    {
        Dialog = dialog;
        RpcService = rpcService;
        //构造函数
        rpcService.CallApi<List<Sys_OrgDTO>>(RpcMethod.GetAllOrg, InvokeOptionUtil.Fast_WaitInvoke_10s, (response) =>
        {
            var Sys_OrgVOs = response.Select(s => s.SimpleMap<Sys_OrgVO>());
            OrgList.LoadFullNodes(Sys_OrgVOs);

            SelectedOrg = response.Find(f => f.BillId == VampirewalCoreContext.GetInstance().GetContext<string>("OrgId")).SimpleMap<Sys_OrgVO>();
        },
        (ex) =>
        {
            //Dialog.ShowPopupWindow(ex.Message, WindowsManager.GetInstance().MainWindow, MessageType.Error);
            Dialog.ShowPopupWindow(new Core.WindoSetting.PopupWindowSetting()
            {
                Message=ex.Message,
                OwnerWindow= WindowsManager.GetInstance().MainWindow,
                Type= Core.WindoSetting.MessageType.Error
            });
        });

        TreeListViewActionsCommand = new IVampirewalCoreEventCommand[]
        {
            new VampirewalCoreEventCommand("SelectedItemChanged", "SelectedItem",new RelayCommand<GeneralTreeContainer<Sys_OrgVO>>((Selected) =>
            {
                var org = Selected.TreeNode;

                if (org.OrgType != 0)
                {
                    Request.OrgId = org.BillId;
                    SelectedOrg = org;
                    BindData(Request);
                }
            }))
        };
    }

    protected override void InitVM()
    {
        OrgList = new TreeBuilderByFullLoad<Sys_OrgVO>();
        Request = new RolePageRequest() { Limit = this.Limit, Page = this.Page };

    }

    private string ViewKey { get; set; }

    protected override IUcViewBase SetView()
    {
        switch (ViewKey)
        {
            case ClientViewKeys.EditSysUserView:
                return WindowsManager.GetInstance().GetView(ViewKey);

            default:
                throw new NotImplementedException();
        }
    }

    #region [     属性     ]

    /// <summary>
    /// 底部页面
    /// </summary>
    [ObservableProperty]
    public IUcViewBase bottomContext;

    public TreeBuilderByFullLoad<Sys_OrgVO> OrgList { get; set; }

    public IVampirewalCoreEventCommand[] TreeListViewActionsCommand { get; set; }

    private RolePageRequest Request { get; set; }

    private Sys_OrgVO SelectedOrg { get; set; }

    #endregion

    #region [     公开方法     ]

    #endregion

    #region [     私有方法     ]
    private void BindData(RolePageRequest request)
    {
        EntityList.Clear();

        RpcService.CallApi<PageContainer<Sys_RoleTemplateDTO>>(RpcMethod.GetPageRoleTemplateList, InvokeOptionUtil.Fast_WaitInvoke_10s, (response) =>
        {
            PageCount = response.PageCount;
            TotalCount = response.TotalCount;

            foreach (var item in response.Data)
            {
                EntityList.Add(item.SimpleMap<Sys_RoleTemplateVO>());
            }
        },
        (ex) =>
        {
            //Dialog.ShowPopupWindow(ex.Message, WindowsManager.GetInstance().MainWindow, MessageType.Error);
            Dialog.ShowPopupWindow(new Core.WindoSetting.PopupWindowSetting()
            {
                Message = ex.Message,
                OwnerWindow = WindowsManager.GetInstance().MainWindow,
                Type = Core.WindoSetting.MessageType.Error
            });
        },
        //new UserRequest() { Limit = this.Limit, Page = this.Page }
        request
        );
    }

    
    #endregion

    #region [     Command命令     ]

    #region [     分页命令     ]

    ///// <summary>
    /////     页码改变命令
    ///// </summary>
    //public RelayCommand<FunctionEventArgs<int>> PageUpdatedCmd =>
    //    new Lazy<RelayCommand<FunctionEventArgs<int>>>(() =>
    //        new RelayCommand<FunctionEventArgs<int>>(PageUpdated)).Value;

    ///// <summary>
    /////     页码改变
    ///// </summary>
    //private void PageUpdated(FunctionEventArgs<int>? info)
    //{
    //    if (info == null)
    //    {
    //        return;
    //    }

    //    EntityList.Clear();

    //    Page = info.Info;
    //    Request.Page = info.Info;
    //    BindData(Request);
    //}

    #endregion



    #endregion

}
