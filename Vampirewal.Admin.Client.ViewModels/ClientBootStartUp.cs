﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ClientBootStartUp
// 创建者：      杨程
// 创建日期：	    2022/12/20 21:09:02

//----------------------------------------------------------------*/

#endregion

using Vampirewal.Admin.Client.ViewModels.Services;

namespace Vampirewal.Admin.Client.ViewModels;

/// <summary>
///
/// </summary>
public partial class ClientBootStartUp : VampirewalBootStartUp
{
    protected override string FirstViewKey => ClientViewKeys.LoginView;

    public override void OnAppExit(bool IsRun)
    {

    }

    public override void OnAppRun(bool IsRun)
    {

    }

    protected override void RegisterService(IServiceCollection services)
    {
        services.AddVampirewalCoreConfig($"{AppDomain.CurrentDomain.BaseDirectory}AppConfig.json", options =>
        {
            options.RegisterOptions<PagingOptions>();
            options.RegisterOptions<UserOptions>();
            options.RegisterOptions<ServerOptions>();
            options.RegisterOptions<UpdateOptions>();
        });

        services.AddVampirewalCoreDialogMessage();
        services.AddSingleton<IVampirewalOperationExcelService, VampirewalOperationExcelService>();
        services.AddSingleton<IRpcService, RpcService>();
        services.AddSingleton<IUpgradeService, UpgradeService>();
    }
}