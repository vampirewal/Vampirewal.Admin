﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    GlobalUsing
// 创建者：      杨程
// 创建日期：	    2022/12/20 21:09:38

//----------------------------------------------------------------*/

#endregion

global using CommunityToolkit.Mvvm.ComponentModel;
global using CommunityToolkit.Mvvm.Input;
global using HandyControl.Data;
global using Microsoft.Extensions.DependencyInjection;
global using SqlSugar;
global using System;
global using System.Collections.Generic;
global using System.Collections.ObjectModel;
global using System.Diagnostics;
global using System.Linq;
global using System.Threading.Tasks;
global using Vampirewal.Admin.Client.ClientRpcServer;
global using Vampirewal.Admin.Client.Model.SystemModels;
global using Vampirewal.Admin.Server.Model.Models.Systems.Role;
global using Vampirewal.Admin.ShareCommon;
global using Vampirewal.Admin.ShareCommon.Requests;
global using Vampirewal.Admin.ShareModel.Models.Systems.AppVersion;
global using Vampirewal.Admin.ShareModel.Models.Systems.ClientMenu;
global using Vampirewal.Admin.ShareModel.Models.Systems.Dic;
global using Vampirewal.Admin.ShareModel.Models.Systems.Org;
global using Vampirewal.Admin.ShareModel.Models.Systems.Role;
global using Vampirewal.Admin.ShareModel.Models.Systems.User;
global using Vampirewal.Core;
global using Vampirewal.Core.Attributes;
global using Vampirewal.Core.Components;
global using Vampirewal.Core.ConfigOptions;
global using Vampirewal.Core.Extensions;
global using Vampirewal.Core.Interface;
global using Vampirewal.Core.Models;
global using Vampirewal.Core.OperationExcelService;
global using Vampirewal.Core.SimpleMVVM;
global using Vampirewal.Core.Tools;