﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    MessageNotifyViewModel
// 创建者：      杨程
// 创建日期：	    2023/1/18 9:54:21

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Client.ViewModels.Controls
{
    /// <summary>
    /// 消息弹窗VM
    /// </summary>
    [VampirewalIoCRegister(ClientViewModelKeys.MessageNotifyViewModel)]
    public partial class MessageNotifyViewModel : ViewModelBase
    {
        /// <summary>
        ///
        /// </summary>
        public MessageNotifyViewModel()
        {
            //构造函数
        }

        public override void PassData(object obj)
        {
            if (obj != null)
            {
                Message = obj.ToString();
            }
            else
            {
                Message = "无消息";
            }
        }

        [ObservableProperty]
        public string message;
    }
}