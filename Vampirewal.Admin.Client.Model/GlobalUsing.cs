﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    GlobalUsing
// 创建者：      杨程
// 创建日期：	    2022/12/26 14:15:07

//----------------------------------------------------------------*/

#endregion

global using System;

global using Vampirewal.Core.Models;
global using Vampirewal.Admin.ShareModel.Models.Systems.Dic;
global using Vampirewal.Core.Attributes;
global using Vampirewal.Core.Models.TreeModel;