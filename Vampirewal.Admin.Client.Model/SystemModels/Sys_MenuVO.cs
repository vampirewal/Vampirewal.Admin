﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_Menu
// 创建者：      杨程
// 创建日期：	    2022/12/15 22:22:18

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Client.Model.SystemModels;

/// <summary>
/// 系统菜单
/// </summary>
///
public partial class Sys_MenuVO : BillBaseModel
{
    private string _MenuName;
    /// <summary>
    /// 菜单名称
    /// </summary>

    public string MenuName
    {
        get
        {
            return _MenuName;
        }
        set
        {
            _MenuName = value;
            OnPropertyChanged();
        }
    }

    private string _Code;

    /// <summary>
    /// 唯一识别编码
    /// </summary>
    public string Code
    {
        get
        {
            return _Code;
        }
        set
        {
            _Code = value;
            OnPropertyChanged();
        }
    }

    private int _MenuType = 0;

    /// <summary>
    /// 菜单类型(0是模块，1是菜单,2是功能按钮)
    /// </summary>
    public int MenuType
    {
        get
        {
            return _MenuType;
        }
        set
        {
            _MenuType = value;
            OnPropertyChanged();
        }
    }

    public string MenuTypeStr
    {
        get
        {
            switch (MenuType)
            {
                case 0:
                    return "顶部模块";

                case 1:
                    return "左侧菜单";

                case 2:
                    return "内部按钮";

                default:
                    return "";
            }
        }
    }

    private bool _IsActive = true;

    /// <summary>
    ///
    /// </summary>
    public bool IsActive
    {
        get
        {
            return _IsActive;
        }
        set
        {
            _IsActive = value;
            OnPropertyChanged();
        }
    }

    private string _Remark;

    /// <summary>
    /// 备注
    /// </summary>
    ///
    public string Remark
    {
        get
        {
            return _Remark;
        }
        set
        {
            _Remark = value;
            OnPropertyChanged();
        }
    }

    private string _ViewPath;

    /// <summary>
    /// 客户端窗体路径
    /// </summary>
    public string ViewPath
    {
        get
        {
            return _ViewPath;
        }
        set
        {
            _ViewPath = value;
            OnPropertyChanged();
        }
    }

    private string _ViewModelPath;

    /// <summary>
    /// ViewModel路径
    /// </summary>
    public string ViewModelPath
    {
        get
        {
            return _ViewModelPath;
        }
        set
        {
            _ViewModelPath = value;
            OnPropertyChanged();
        }
    }

    private string _Permission;

    /// <summary>
    /// 权限标识
    /// </summary>
    public string Permission
    {
        get
        {
            return _Permission;
        }
        set
        {
            _Permission = value;
            OnPropertyChanged();
        }
    }
}

public partial class Sys_MenuVO : ITreeNode
{
    private string _ListId;

    /// <summary>
    /// 当前层级ID
    /// </summary>
    public string ListId
    {
        get
        {
            return _ListId;
        }
        set
        {
            _ListId = value;
            OnPropertyChanged();
        }
    }

    private string _ParentId;

    /// <summary>
    /// 父级ID
    /// </summary>
    public string ParentId
    {
        get
        {
            return _ParentId;
        }
        set
        {
            _ParentId = value;
            OnPropertyChanged();
        }
    }

    public object GetCurrentNodeMark()
    {
        return ListId;
    }

    public object GetParentNodeMark()
    {
        return ParentId;
    }

    public bool IsParentNode(ITreeNode parentNode)
    {
        if (parentNode is Sys_MenuVO node)
            return ParentId == node.ListId;
        return false;
    }

    public bool IsSameNode(ITreeNode node)
    {
        if (node is Sys_MenuVO tmpNode)
            return ListId == tmpNode.ListId;
        return false;
    }

    public bool IsSameParent(ITreeNode node)
    {
        if (node is Sys_MenuVO tmpNode)
            return ParentId == tmpNode.ParentId;
        return false;
    }

    public bool IsSubNode(ITreeNode subNode)
    {
        if (subNode is Sys_MenuVO tmpNode)
            return tmpNode.ParentId == ListId;
        return false;
    }

    public void SetParentNodeMark(object parentObj)
    {
        ParentId = parentObj.ToString();
    }
}