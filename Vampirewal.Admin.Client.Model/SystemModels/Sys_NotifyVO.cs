﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_NotifyVO
// 创建者：      杨程
// 创建日期：	    2023/1/19 10:43:28

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Client.Model.SystemModels;

/// <summary>
/// 系统通知VO
/// </summary>
public partial class Sys_NotifyVO : BillBaseModel
{
    private int _NotifyType;

    /// <summary>
    /// 通知类型(0是一般通知，1是成功通知，2是错误通知,3是临时通知)
    /// </summary>
    public int NotifyType
    { get => _NotifyType; set { _NotifyType = value; OnPropertyChanged(); } }

    private string _NotifyMsg;

    /// <summary>
    /// 通知内容
    /// </summary>
    public string NotifyMsg
    { get => _NotifyMsg; set { _NotifyMsg = value; OnPropertyChanged(); } }

    private bool _IsReceived;

    /// <summary>
    /// 是否已接收
    /// </summary>
    public bool IsReceived
    { get => _IsReceived; set { _IsReceived = value; OnPropertyChanged(); } }

    private DateTime? _ReceivedTime;

    /// <summary>
    /// 接收时间
    /// </summary>
    public DateTime? ReceivedTime
    { get => _ReceivedTime; set { _ReceivedTime = value; OnPropertyChanged(); } }

    private string _SendUserId;

    /// <summary>
    /// 发送人ID
    /// </summary>
    public string SendUserId
    { get => _SendUserId; set { _SendUserId = value; OnPropertyChanged(); } }

    private string _ReceiveUserId;

    /// <summary>
    /// 接收人ID
    /// </summary>
    public string ReceiveUserId
    { get => _ReceiveUserId; set { _ReceiveUserId = value; OnPropertyChanged(); } }

    private string _OrgId;

    /// <summary>
    /// 机构ID
    /// </summary>
    public string OrgId
    { get => _OrgId; set { _OrgId = value; OnPropertyChanged(); } }

    private string _TenantId;

    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId
    { get => _TenantId; set { _TenantId = value; OnPropertyChanged(); } }
}