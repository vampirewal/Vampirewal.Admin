﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_RoleTemplate
// 创建者：      杨程
// 创建日期：	    2022/12/20 15:09:35

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.Model.Models.Systems.Role;

/// <summary>
/// 角色模版
/// </summary>
public class Sys_RoleTemplateVO : BillBaseModel
{
    private string _Code;
    private int _DataScope;
    private bool _IsActive = true;
    private bool _IsAdmin = false;
    private string _RoleName;

    private string _TenantId;

    /// <summary>
    /// 编码
    /// </summary>
    public string Code
    {
        get
        {
            return _Code;
        }
        set
        {
            _Code = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    /// 角色数据权限(0全部，1是当前机构及下级机构，2是当前机构，3是无只能看自己)
    /// </summary>
    public int DataScope
    {
        get
        {
            return _DataScope;
        }
        set
        {
            _DataScope = value;
            OnPropertyChanged();
        }
    }

    public string DataScopeStr
    {
        get
        {
            string result = string.Empty;

            switch (DataScope)
            {
                case 0:
                    result = "全部";
                    break;

                case 1:
                    result = "当前机构及下级机构";
                    break;

                case 2:
                    result = "当前机构";
                    break;

                case 3:
                    result = "无数据权限，只能看自己";
                    break;
            }

            return result;
        }
    }

    public bool IsActive
    {
        get { return _IsActive; }
        set { _IsActive = value; OnPropertyChanged(); }
    }

    /// <summary>
    /// 是否管理员
    /// </summary>
    public bool IsAdmin
    {
        get
        {
            return _IsAdmin;
        }
        set
        {
            _IsAdmin = value;
            OnPropertyChanged();
        }
    }

    /// <summary>
    /// 角色名称
    /// </summary>
    public string RoleName
    {
        get { return _RoleName; }
        set { _RoleName = value; OnPropertyChanged(); }
    }

    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId
    {
        get => _TenantId;
        set
        {
            _TenantId = value;
            OnPropertyChanged();
        }
    }
}