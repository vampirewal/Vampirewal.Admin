﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_DicVO
// 创建者：      杨程
// 创建日期：	    2023/2/8 13:47:49

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Client.Model.SystemModels;

/// <summary>
///
/// </summary>
public partial class Sys_DicVO : DetailItemChangeBaseModel<Sys_DicDTO>
{
    /// <summary>
    ///
    /// </summary>
    public Sys_DicVO(Sys_DicDTO dto) : base(dto)
    {
        //构造函数
    }

    private int _DicValue = 0;

    /// <summary>
    /// 字典值
    /// </summary>
    [ListenDetailChange]
    public int DicValue
    {
        get
        {
            return _DicValue;
        }
        set
        {
            _DicValue = value;
            OnPropertyChanged();
        }
    }

    private string _Description;

    /// <summary>
    /// 字典描述
    /// </summary>
    [ListenDetailChange]
    public string Description
    {
        get
        {
            return _Description;
        }
        set
        {
            _Description = value;
            OnPropertyChanged();
        }
    }

    private bool _IsActive = true;

    /// <summary>
    /// 是否启用
    /// </summary>
    [ListenDetailChange]
    public bool IsActive
    {
        get
        {
            return _IsActive;
        }
        set
        {
            _IsActive = value;
            OnPropertyChanged();
        }
    }

    public override void LoadDTO(Sys_DicDTO dto)
    {
        this.DtlId = dto.DtlId;
        this.BillId = dto.BillId;
        this.DicValue = dto.DicValue;
        this.Description = dto.Description;
        this.IsActive = dto.IsActive;
    }

    public Sys_DicDTO ToDto()
    {
        Sys_DicDTO dic = new Sys_DicDTO()
        {
            BillId = this.BillId,
            Description = this.Description,
            DicValue = this.DicValue,
            IsActive = this.IsActive,
        };

        return dic;
    }
}