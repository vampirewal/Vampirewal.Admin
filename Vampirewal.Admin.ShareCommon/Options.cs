﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：	服务端和客户端共享的配置项
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Options
// 创建者：      杨程
// 创建日期：	    2023/1/31 15:39:01

//----------------------------------------------------------------*/

#endregion

using Vampirewal.Core.Interface;

namespace Vampirewal.Admin.ShareCommon;

#region [     运行环境配置     ]

/// <summary>
/// 运行环境配置
/// </summary>
public class EnvironmentOptions : IOptions
{
    public bool IsDev { get; set; } = true;

    public bool IsGenerateRpcMethod { get; set; } = false;
}

#endregion

#region [     更新配置     ]

/// <summary>
/// 更新配置
/// </summary>
public class UpdateOptions : IOptions
{
    public string UpdateName { get; set; } = "";

    public string ServerIp { get; set; } = "";

    public int ServerPort { get; set; } = 0;

    public string AppToken { get; set; } = "";
}

#endregion

#region [     当前登陆账号信息     ]

/// <summary>
/// 当前登陆账号信息
/// </summary>
public class UserOptions : IOptions
{
    /// <summary>
    /// 登陆名
    /// </summary>
    public string LoginName { get; set; } = "";
    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get; set; } = "";
    /// <summary>
    /// 是否记住密码
    /// </summary>
    public bool IsRemeber { get; set; } = false;
    /// <summary>
    /// 是否自动登陆
    /// </summary>
    public bool IsAutoLogin { get; set; }=false;
}

#endregion

#region [     服务器配置     ]

/// <summary>
/// 服务器配置
/// </summary>
public class ServerOptions : IOptions
{
    public string RpcServerAddress { get; set; } = "";

    public List<ServerInfo> Servers { get; set; } = new List<ServerInfo>();
}

public class ServerInfo
{
    /// <summary>
    /// 服务器名称
    /// </summary>
    public string Name { get; set; } = "";

    /// <summary>
    /// IP地址
    /// </summary>
    public string IpAddress { get; set; } = "";

    /// <summary>
    /// 端口号
    /// </summary>
    public int Port { get; set; } = 0;

    /// <summary>
    /// 域名
    /// </summary>
    public string EntryUrl { get; set; }
}

#endregion

#region [     文件存储配置     ]

/// <summary>
/// 文件存储配置
/// </summary>
public class FileStorageOptions : IOptions
{
    public List<FileStorageInfo> FileStorageInfos { get; set; } = new List<FileStorageInfo>();
}

public class FileStorageInfo
{
    /// <summary>
    /// 存储类型名称
    /// </summary>
    public string StorageName { get; set; }

    /// <summary>
    /// 存储路径
    /// </summary>
    public string StoragePath { get; set; }
}

#endregion

#region 打包配置

/// <summary>
/// 打包配置
/// </summary>
public class PackageOptions : IOptions
{
    public string TargetPath { get; set; }

    public List<string> TargetFiles { get; set;} = new List<string>();
}

#endregion