﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    HeaderAttribute
// 创建者：      杨程
// 创建日期：	    2022/12/27 13:20:31

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
///
/// </summary>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public partial class HeaderAttribute : Attribute
{
    /// <summary>
    ///
    /// </summary>
    public HeaderAttribute(string header)
    {
        Header = header;
        //构造函数
    }

    /// <summary>
    /// 显示的列头
    /// </summary>
    public string Header { get; private set; }

    /// <summary>
    /// 列头宽度
    /// </summary>
    public double HeaderWidth { get; set; }

    /// <summary>
    /// 内容,对齐方式
    /// </summary>
    public AlignmentMode CellAlignmentMode { get; set; } = AlignmentMode.Center;

    /// <summary>
    /// 转换器
    /// </summary>
    public Type Converter { get; set; }
}

/// <summary>
/// 列表预览特性
/// </summary>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public class GeneralListHeaderAttribute : HeaderAttribute
{
    /// <summary>
    /// 是否为进入编辑页项【目前只支持一个】
    /// </summary>
    public bool InEdit { get; set; }

    /// <summary>
    /// 操作对应的标识，需要唯一性
    /// </summary>
    public string OperatorToken { get; set; }

    /// <summary>
    /// 根据<paramref name="header"/>创建指定名称的特性
    /// </summary>
    /// <param name="header">列头显示名</param>
    public GeneralListHeaderAttribute(string header) : base(header)
    {
    }
}

/// <summary>
/// 对齐方式
/// </summary>
public enum AlignmentMode
{
    Left,
    Center,
    Right,
    Top,
    Bottom
}