﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    PageRequest
// 创建者：      杨程
// 创建日期：	    2023/1/31 10:28:02

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
/// 分页请求模型
/// </summary>
public class PageRequest
{
    private int _Page = 1;

    /// <summary>
    /// 当前页
    /// </summary>
    public int Page
    { get => _Page; set { _Page = value; } }

    private int _Limit = 20;

    /// <summary>
    /// 每页数
    /// </summary>
    public int Limit
    { get => _Limit; set { _Limit = value; } }

    private string _KeyWord;

    /// <summary>
    /// 关键词
    /// </summary>
    public string KeyWord
    {
        get { return _KeyWord; }
        set { _KeyWord = value; }
    }
}