﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    ViewModekKeys
// 创建者：      杨程
// 创建日期：	    2022/12/15 12:51:19

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Server.Common;

/// <summary>
/// ViewModel的Keys
/// </summary>
public partial class ServerViewModelKeys
{
    /// <summary>
    /// 主窗体VM
    /// </summary>
    public const string MainViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.MainView.MainViewModel";

    #region [     系统管理     ]

    /// <summary>
    /// 字典管理VM
    /// </summary>
    public const string DicManagerViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.SystemManager.DicManagerViewModel";
    /// <summary>
    /// 编辑字典VM
    /// </summary>
    public const string EditDicViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.SystemManager.EditDicViewModel";

    /// <summary>
    /// 菜单管理VM
    /// </summary>
    public const string ClientMenuManagerViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.SystemManager.ClientMenuManagerViewModel";

    /// <summary>
    /// 编辑菜单VM
    /// </summary>
    public const string EditClientMenuViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.SystemManager.EditClientMenuViewModel";

    /// <summary>
    /// 租户管理VM
    /// </summary>
    public const string TenantManagerViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.SystemManager.TenantManagerViewModel";

    /// <summary>
    /// 编辑租户VM
    /// </summary>
    public const string EditTenantViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.SystemManager.EditTenantViewModel";

    #endregion

    #region [     组织机构管理     ]

    /// <summary>
    /// 机构管理VM
    /// </summary>
    public const string OrgManagerViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager.OrgManagerViewModel";

    /// <summary>
    /// 编辑机构页VM
    /// </summary>
    public const string EditOrgViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager.EditOrgViewModel";

    /// <summary>
    /// 人员管理页
    /// </summary>
    public const string UserManagerViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager.UserManagerViewModel";

    /// <summary>
    /// 编辑人员VM
    /// </summary>
    public const string EditUserViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager.EditUserViewModel";

    /// <summary>
    /// 角色管理页VM
    /// </summary>
    public const string RoleManagerViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager.RoleManagerViewModel";

    /// <summary>
    /// 编辑角色页VM
    /// </summary>
    public const string EditRoleViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager.EditRoleViewModel";

    /// <summary>
    /// 给角色分配权限VM
    /// </summary>
    public const string ConfigurePermissionViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.OrganizationManager.ConfigurePermissionViewModel";
    #endregion

    #region [     流程引擎管理     ]

    /// <summary>
    /// 流程引擎管理页面VM
    /// </summary>
    public const string FlowEngineManagerViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.FlowEngineManager.FlowEngineManagerViewModel";

    /// <summary>
    /// 流程引擎编辑页面VM
    /// </summary>
    public const string EditFlowEngineViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.FlowEngineManager.EditFlowEngineViewModel";
    #endregion

    #region [     首页管理     ]
    /// <summary>
    /// 当前在线人员VM
    /// </summary>
    public const string OnlineUserViewModel = "Vampirewal.Admin.Server.ViewModels.ViewModel.HomeManager.OnlineUserViewModel";

    #endregion
}
