﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    PageContainer
// 创建者：      杨程
// 创建日期：	    2023/1/31 10:27:00

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
/// 分页模型
/// </summary>
public class PageContainer
{
    private long _Count;

    /// <summary>
    /// 记录数
    /// </summary>
    public long Count
    { get => _Count; set { _Count = value; } }

    private int _PageCount = 0;

    /// <summary>
    /// 分页数
    /// </summary>
    public int PageCount
    { get => _PageCount; set { _PageCount = value; } }

    private int _TotalCount;

    /// <summary>
    /// 数据总条数
    /// </summary>
    public int TotalCount
    { get => _TotalCount; set { _TotalCount = value; } }

    /// <summary>
    /// 数据
    /// </summary>
    public string Data { get; set; }
}

/// <summary>
/// 分页模型
/// </summary>
/// <typeparam name="T"></typeparam>
public class PageContainer<T>
{
    private long _Count;

    /// <summary>
    /// 记录数
    /// </summary>
    public long Count
    { get => _Count; set { _Count = value; } }

    private int _PageCount = 0;

    /// <summary>
    /// 分页数
    /// </summary>
    public int PageCount
    { get => _PageCount; set { _PageCount = value; } }

    private int _TotalCount;

    /// <summary>
    /// 数据总条数
    /// </summary>
    public int TotalCount
    { get => _TotalCount; set { _TotalCount = value; } }

    /// <summary>
    /// 数据
    /// </summary>
    public List<T> Data { get; set; }
}