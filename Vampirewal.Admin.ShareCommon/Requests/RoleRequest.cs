﻿#region [     文件信息     ]

/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.ShareCommon.Requests
 * 唯一标识：032b7700-b048-4054-bf86-43e1faa97ba9
 * 文件名：RoleRequest
 * 当前用户域：VAMPIREWAL
 *
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/3 13:56:19
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion [     文件信息     ]

namespace Vampirewal.Admin.ShareCommon.Requests;

/// <summary>
/// 角色管理分页请求
/// </summary>
public partial class RolePageRequest : PageRequest
{
    /// <summary>
    /// 机构ID
    /// </summary>
    public string OrgId { get; set; }

}