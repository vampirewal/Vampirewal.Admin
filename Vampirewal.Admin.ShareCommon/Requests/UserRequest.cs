﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    UserRequest
// 创建者：      杨程
// 创建日期：	    2023/1/31 10:28:37

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareCommon.Requests;

/// <summary>
/// 用户查询请求类
/// </summary>
public partial class UserRequest : PageRequest
{
    /// <summary>
    /// 机构ID
    /// </summary>
    public string OrgId { get; set; }

    /// <summary>
    /// 机构名称
    /// </summary>
    public string OrgName { get; set; }

    /// <summary>
    ///
    /// </summary>
    public string UserName { get; set; }
}