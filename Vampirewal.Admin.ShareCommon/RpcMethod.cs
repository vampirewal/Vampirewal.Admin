
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	对应服务端的Rpc方法
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    RpcMethod
// 创建者：      Vampirewal
// 创建日期：	    2023/3/13 15:08:13

//----------------------------------------------------------------*/
#endregion


namespace Vampirewal.Admin.ShareCommon;

/// <summary>
/// RPC接口方法地址
/// </summary>
public sealed class RpcMethod
{

    #region [     FlowEngineSerivce-流程引擎RPC服务     ]
    /// <summary>
    /// 新增流程设计
    /// </summary>
    public const string FlowDesignCreate = "vampirewal.admin.server.rpcservices.service.flowengineserivce.flowdesigncreate";

    #endregion

    #region [     DicService-字典RPC服务     ]
    /// <summary>
    /// 新增字典类型
    /// </summary>
    public const string InsertCategory = "vampirewal.admin.server.rpcservices.service.dicservice.insertcategory";

    /// <summary>
    /// 新增字典值
    /// </summary>
    public const string InsertDicValue = "vampirewal.admin.server.rpcservices.service.dicservice.insertdicvalue";

    /// <summary>
    /// 删除字典类型
    /// </summary>
    public const string DeleteDicCategory = "vampirewal.admin.server.rpcservices.service.dicservice.deletediccategory";

    /// <summary>
    /// 查询所有字典类型
    /// </summary>
    public const string GetAllDicCategory = "vampirewal.admin.server.rpcservices.service.dicservice.getalldiccategory";

    /// <summary>
    /// 分页查询字典类型
    /// </summary>
    public const string GetPageListDicCategory = "vampirewal.admin.server.rpcservices.service.dicservice.getpagelistdiccategory";

    /// <summary>
    /// 通过字典类型BillId查询字典值
    /// </summary>
    public const string GetDicValueByCategory = "vampirewal.admin.server.rpcservices.service.dicservice.getdicvaluebycategory";

    /// <summary>
    /// 通过字典类型Id查询字典值并转换成客户端使用的枚举值
    /// </summary>
    public const string GetDicEnumberCreditListByCategoryId = "vampirewal.admin.server.rpcservices.service.dicservice.getdicenumbercreditlistbycategoryid";

    #endregion

    #region [     OrgService-机构RPC服务     ]
    /// <summary>
    /// 通过Id查询机构
    /// </summary>
    public const string GetOrgById = "vampirewal.admin.server.rpcservices.service.orgservice.getorgbyid";

    /// <summary>
    /// 查询当前租户下所有的机构
    /// </summary>
    public const string GetAllOrg = "vampirewal.admin.server.rpcservices.service.orgservice.getallorg";

    /// <summary>
    /// 查询当前机构的下级机构
    /// </summary>
    public const string GetSubordinateOrg = "vampirewal.admin.server.rpcservices.service.orgservice.getsubordinateorg";

    #endregion

    #region [     SystemService-系统RPC接口     ]
    /// <summary>
    /// 验证租户是否能用
    /// </summary>
    public const string CheckTenant = "vampirewal.admin.server.rpcservices.service.systemservice.checktenant";

    /// <summary>
    /// 获取当前用户的页面权限
    /// </summary>
    public const string GetPermissions = "vampirewal.admin.server.rpcservices.service.systemservice.getpermissions";

    /// <summary>
    /// 获取当前用户的菜单
    /// </summary>
    public const string GetLoginUserMenus = "vampirewal.admin.server.rpcservices.service.systemservice.getloginusermenus";

    /// <summary>
    /// 获取当前客户端最新版本
    /// </summary>
    public const string GetLastVersion = "vampirewal.admin.server.rpcservices.service.systemservice.getlastversion";

    /// <summary>
    /// 新增客户端版本
    /// </summary>
    public const string CreateNewVersion = "vampirewal.admin.server.rpcservices.service.systemservice.createnewversion";

    /// <summary>
    /// 获取附件信息
    /// </summary>
    public const string GetAttachmentById = "vampirewal.admin.server.rpcservices.service.systemservice.getattachmentbyid";

    #endregion

    #region [     UserService-RPC用户服务     ]
    /// <summary>
    /// 新增用户
    /// </summary>
    public const string CreateUser = "vampirewal.admin.server.rpcservices.service.userservice.createuser";

    /// <summary>
    /// 删除用户(将State字段修改为2)
    /// </summary>
    public const string DeleteUser = "vampirewal.admin.server.rpcservices.service.userservice.deleteuser";

    /// <summary>
    /// 修改用户信息
    /// </summary>
    public const string UpdateUser = "vampirewal.admin.server.rpcservices.service.userservice.updateuser";

    /// <summary>
    /// 分页查询用户
    /// </summary>
    public const string GetPageListUser = "vampirewal.admin.server.rpcservices.service.userservice.getpagelistuser";

    /// <summary>
    /// 通过BillId查找用户
    /// </summary>
    public const string GetUserById = "vampirewal.admin.server.rpcservices.service.userservice.getuserbyid";

    /// <summary>
    /// 根据角色ID获取用户列表
    /// </summary>
    public const string GetUserListByRoleId = "vampirewal.admin.server.rpcservices.service.userservice.getuserlistbyroleid";

    /// <summary>
    /// 根据机构ID获取当前部门的负责人信息
    /// </summary>
    public const string GetCurrentDeptLeaderByOrgId = "vampirewal.admin.server.rpcservices.service.userservice.getcurrentdeptleaderbyorgid";

    #endregion

    #region [     FlowEngineService-流程引擎RPC服务     ]
    /// <summary>
    /// 查询当前租户下的所有流程实例
    /// </summary>
    public const string GetFlowList = "vampirewal.admin.server.rpcservices.service.flowengineservice.getflowlist";

    /// <summary>
    /// 查询当前租户单个流程实例
    /// </summary>
    public const string GetFlowBaseInfo = "vampirewal.admin.server.rpcservices.service.flowengineservice.getflowbaseinfo";

    /// <summary>
    /// 通过流程ID查询流程明细
    /// </summary>
    public const string GetFlowStepById = "vampirewal.admin.server.rpcservices.service.flowengineservice.getflowstepbyid";

    /// <summary>
    /// 新增流程基本信息
    /// </summary>
    public const string CreateFlowBaseInfo = "vampirewal.admin.server.rpcservices.service.flowengineservice.createflowbaseinfo";

    /// <summary>
    /// 新增流程步骤信息
    /// </summary>
    public const string CreateFlowStep = "vampirewal.admin.server.rpcservices.service.flowengineservice.createflowstep";

    /// <summary>
    /// 修改流程基本信息
    /// </summary>
    public const string UpdateFlowBaseInfo = "vampirewal.admin.server.rpcservices.service.flowengineservice.updateflowbaseinfo";

    #endregion

    #region [     RoleService-角色RPC服务     ]
    /// <summary>
    /// 新增角色模版
    /// </summary>
    public const string CreateRole = "vampirewal.admin.server.rpcservices.service.roleservice.createrole";

    /// <summary>
    /// 操作用户角色关联信息(新增或更新)
    /// </summary>
    public const string OperationUserRole = "vampirewal.admin.server.rpcservices.service.roleservice.operationuserrole";

    /// <summary>
    /// 修改角色模版
    /// </summary>
    public const string UpdateRole = "vampirewal.admin.server.rpcservices.service.roleservice.updaterole";

    /// <summary>
    /// 删除角色模版
    /// </summary>
    public const string DeleteRole = "vampirewal.admin.server.rpcservices.service.roleservice.deleterole";

    /// <summary>
    /// 删除用户角色关联信息
    /// </summary>
    public const string DeleteUserRole = "vampirewal.admin.server.rpcservices.service.roleservice.deleteuserrole";

    /// <summary>
    /// 查询当前租户下的所有角色模版(租户管理员才能使用)
    /// </summary>
    public const string QueryRoleTemplate = "vampirewal.admin.server.rpcservices.service.roleservice.queryroletemplate";

    /// <summary>
    /// 分页获取角色模版(租户管理员才能使用)
    /// </summary>
    public const string GetPageRoleTemplateList = "vampirewal.admin.server.rpcservices.service.roleservice.getpageroletemplatelist";

    /// <summary>
    /// 根据机构ID获取当前机构下的角色模版(租户管理员才能使用)
    /// </summary>
    public const string GetRoleTemplatesByOrgId = "vampirewal.admin.server.rpcservices.service.roleservice.getroletemplatesbyorgid";

    /// <summary>
    /// 获取当前用户的数据权限
    /// </summary>
    public const string GetRoleDataScope = "vampirewal.admin.server.rpcservices.service.roleservice.getroledatascope";

    #endregion

    #region [     LoginService-登陆服务     ]
    /// <summary>
    /// 登陆
    /// </summary>
    public const string Login = "vampirewal.admin.server.rpcservices.service.loginservice.login";

    /// <summary>
    /// 修改密码
    /// </summary>
    public const string ChangePassWord = "vampirewal.admin.server.rpcservices.service.loginservice.changepassword";

    /// <summary>
    /// 登出
    /// </summary>
    public const string LoginOut = "vampirewal.admin.server.rpcservices.service.loginservice.loginout";

    #endregion

    #region [     ClientNotifyServicee-客户端通知RPC服务     ]
    /// <summary>
    /// 给其他客户端发送通知
    /// </summary>
    public const string SendMessage = "vampirewal.admin.server.rpcservices.service.clientnotifyservicee.sendmessage";

    #endregion

}
