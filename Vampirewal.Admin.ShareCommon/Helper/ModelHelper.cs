﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ModelHelper
// 创建者：      杨程
// 创建日期：	    2023/1/7 17:18:25

//----------------------------------------------------------------*/

#endregion

using System.Reflection;
using Vampirewal.Core;
using Vampirewal.Core.Models;

namespace Vampirewal.Admin.ShareCommon.Helper;

/// <summary>
/// 模型帮助类
/// </summary>
public partial class ModelHelper
{
    /// <summary>
    /// 服务端创建model并赋初始值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Sys_User"></param>
    /// <returns></returns>
    public static T? ServerCreateModel<T>(dynamic Sys_User) where T : TopModel, new()
    {
        Type type = typeof(T);

        T? model = Activator.CreateInstance(type) as T;

        if (model != null)
        {
            PropertyInfo[]? Props = type.GetProperties();

            foreach (PropertyInfo prop in Props)
            {
                if (prop.Name == "OrgId")
                {
                    prop.SetValue(model, Sys_User.OrgId);
                }

                if (prop.Name == "OrgName")
                {
                    prop.SetValue(model, Sys_User.OrgName);
                }

                if (prop.Name == "CreateBy")
                {
                    prop.SetValue(model, Sys_User.Name);
                }

                if (prop.Name == "CreateUserId")
                {
                    prop.SetValue(model, Sys_User.BillId);
                }

                if (prop.Name == "CreateTime")
                {
                    prop.SetValue(model, DateTime.Now);
                }

                if (prop.Name == "TenantId")
                {
                    prop.SetValue(model, Sys_User.TenantId);
                }
            }
        }

        return model;
    }

    /// <summary>
    /// 客户端创建model并赋初始值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T? ClientCreateModel<T>() where T : TopModel, new()
    {
        Type type = typeof(T);

        T? model = Activator.CreateInstance(type) as T;

        if (model != null)
        {
            PropertyInfo[]? Props = type.GetProperties();

            foreach (PropertyInfo prop in Props)
            {
                if (prop.Name == "OrgId")
                {
                    prop.SetValue(model, VampirewalCoreContext.GetInstance().GetContext<string>("OrgId"));
                }

                if (prop.Name == "OrgName")
                {
                    prop.SetValue(model, VampirewalCoreContext.GetInstance().GetContext<string>("OrgName"));
                }

                if (prop.Name == "CreateBy")
                {
                    prop.SetValue(model, VampirewalCoreContext.GetInstance().GetContext<string>("Name"));
                }

                if (prop.Name == "CreateUserId")
                {
                    prop.SetValue(model, VampirewalCoreContext.GetInstance().GetContext<string>("BillId"));
                }

                if (prop.Name == "CreateTime")
                {
                    prop.SetValue(model, DateTime.Now);
                }

                if (prop.Name == "TenantId")
                {
                    prop.SetValue(model, VampirewalCoreContext.GetInstance().GetContext<string>("TenantId"));
                }
            }
        }

        return model;
    }
}