﻿// MIT 许可证
//
// 版权 © 2020-present Vampirewal
//
// 特此免费授予任何获得本软件副本和相关文档文件（下称“软件”）的人不受限制地处置该软件的权利，
// 包括不受限制地使用、复制、修改、合并、发布、分发、转授许可和/或出售该软件副本，
// 以及再授权被配发了本软件的人如上的权利，须在下列条件下：
//
// 上述版权声明和本许可声明应包含在该软件的所有副本或实质成分中。
//
// 本软件是“如此”提供的，没有任何形式的明示或暗示的保证，包括但不限于对适销性、特定用途的适用性和不侵权的保证。
// 在任何情况下，作者或版权持有人都不对任何索赔、损害或其他责任负责，无论这些追责来自合同、侵权或其它行为中，
// 还是产生于、源于或有关于本软件以及本软件的使用或其它处置。

#region << 文件说明 >>
/*----------------------------------------------------------------
 * 命名空间：Vampirewal.Admin.ShareCommon.Services
 * 唯一标识：da9b53f6-c6e7-4084-bc31-4bc0d87cac17
 * 文件名：IApplicationService
 * 
 * 创建者：杨程
 * 电子邮箱：235160615@qq.com
 * 创建时间：2024/8/26 11:27:33
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion << 文件说明 >>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Vampirewal.Admin.ShareCommon;

/// <summary>
/// 
/// </summary>
public interface IApplicationService
{
    /// <summary>
    /// 执行异步跨UI线程
    /// </summary>
    /// <param name="act"></param>
    void BeginInvoke(Action act);

    /// <summary>
    /// 关闭应用程序
    /// </summary>
    void Shutdown();
}