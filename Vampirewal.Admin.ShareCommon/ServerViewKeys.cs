﻿#region [     文件信息     ]

/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.ShareCommon
 * 唯一标识：b0d2e6a4-359d-41d8-80b7-b4d96350552a
 * 文件名：ServerViewKeys
 * 当前用户域：VAMPIREWAL
 *
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/2/27 18:45:25
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion [     文件信息     ]

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
///服务端窗体Key值
/// </summary>
public partial class ServerViewKeys
{
    /// <summary>
    /// 主页面
    /// </summary>
    public const string MainView = "Vampirewal.Admin.Server.MainView";

    #region [     系统管理页面地址     ]

    /// <summary>
    /// 字典管理
    /// </summary>
    public const string DicManagerView = "Vampirewal.Admin.Server.Views.SystemManagerViews.Dic.DicManagerView";
    /// <summary>
    /// 字典编辑页
    /// </summary>
    public const string EditDicView = "Vampirewal.Admin.Server.Views.SystemManagerViews.Dic.EditDicView";

    /// <summary>
    /// 客户端菜单配置管理
    /// </summary>
    public const string ClientMenuManagerView = "Vampirewal.Admin.Server.Views.SystemManagerViews.ClientMenu.ClientMenuManagerView";

    /// <summary>
    /// 菜单编辑页
    /// </summary>
    public const string EditClientMenuView = "Vampirewal.Admin.Server.Views.SystemManagerViews.ClientMenu.EditClientMenuView";

    /// <summary>
    /// 租户管理页
    /// </summary>
    public const string TenantManagerView = "Vampirewal.Admin.Server.Views.SystemManagerViews.Tenant.TenantManagerView";

    /// <summary>
    /// 租户编辑页面
    /// </summary>
    public const string EditTenanView = "Vampirewal.Admin.Server.Views.SystemManagerViews.Tenant.EditTenanView";
    #endregion

    #region [     组织机构管理页面地址     ]

    /// <summary>
    /// 机构管理页面
    /// </summary>
    public const string OrgManagerView = "Vampirewal.Admin.Server.Views.OrganizationManagerViews.Org.OrgManagerView";

    /// <summary>
    /// 机构编辑页
    /// </summary>
    public const string EditOrgView = "Vampirewal.Admin.Server.Views.OrganizationManagerViews.Org.EditOrgView";

    /// <summary>
    /// 人员管理页面
    /// </summary>
    public const string UserManagerView = "Vampirewal.Admin.Server.Views.OrganizationManagerViews.User.UserManagerView";
    /// <summary>
    /// 人员编辑页面
    /// </summary>
    public const string EditUserView = "Vampirewal.Admin.Server.Views.OrganizationManagerViews.User.EditUserView";

    /// <summary>
    /// 角色管理页面
    /// </summary>
    public const string RoleManagerView = "Vampirewal.Admin.Server.Views.OrganizationManagerViews.Role.RoleManagerView";

    /// <summary>
    /// 角色编辑页面
    /// </summary>
    public const string EditRoleView = "Vampirewal.Admin.Server.Views.OrganizationManagerViews.Role.EditRoleView";

    /// <summary>
    /// 给角色分配权限页面
    /// </summary>
    public const string ConfigurePermissionView = "Vampirewal.Admin.Server.Views.OrganizationManagerViews.Role.ConfigurePermissionView";

    #endregion

    #region [     流程引擎管理页面    ]
    /// <summary>
    /// 流程引擎管理页面
    /// </summary>
    public const string FlowEngineManagerView = "Vampirewal.Admin.Server.Views.FlowEngineManagerViews.FlowEngineManagerView";

    /// <summary>
    /// 流程引擎编辑页面
    /// </summary>
    public const string EditFlowEngineView = "Vampirewal.Admin.Server.Views.FlowEngineManagerViews.EditFlowEngineView";

    #endregion

    #region [     首页管理     ]
    /// <summary>
    /// 在线人员View
    /// </summary>
    public const string OnlineUserView = "Vampirewal.Admin.Server.Views.HomeManagerViews.OnlineUserView";

    #endregion
}