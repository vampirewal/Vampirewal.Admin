﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ViewKeys
// 创建者：      杨程
// 创建日期：	    2022/12/20 21:11:36

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
///
/// </summary>
public partial class ClientViewKeys
{
    /// <summary>
    /// 登陆窗体
    /// </summary>
    public const string LoginView = "Vampirewal.Admin.Client.LoginView";

    /// <summary>
    /// 主窗体
    /// </summary>
    public const string MainView = "Vampirewal.Admin.Client.MainView";

    /// <summary>
    /// 通用列表页
    /// </summary>
    public const string GeneralListView = "Vampirewal.Admin.Client.Views.GeneralListView";

    /// <summary>
    /// 通知消息弹窗
    /// </summary>
    public const string MessageNotifyView = "Vampirewal.Admin.Client.Controls.MessageNotifyView";

    #region [     系统基础页面(请勿删除)     ]

    #region 用户管理

    /// <summary>
    /// 用户管理列表页
    /// </summary>
    public const string SysUserManagerView = "Vampirewal.Admin.Client.SystemViews.UserManager.SysUserManagerView";

    /// <summary>
    /// 用户管理编辑页
    /// </summary>
    public const string EditSysUserView = "Vampirewal.Admin.Client.SystemViews.UserManager.EditSysUserView";

    #endregion

    #region 角色管理

    /// <summary>
    /// 客户端角色管理列表页
    /// </summary>
    public const string RoleManagerView = "Vampirewal.Admin.Client.SystemViews.RoleManager.RoleManagerView";

    #endregion

    #endregion
}