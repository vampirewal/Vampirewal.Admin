﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    SugarTableKeys
// 创建者：      杨程
// 创建日期：	    2022/12/15 13:36:15

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
///
/// </summary>
public partial class SugarTableKeys
{
    #region [     系统管理模块     ]

    /// <summary>
    /// 字典类别
    /// </summary>
    public const string Sys_DicCategory = "Sys_DicCategory";

    /// <summary>
    /// 字典值
    /// </summary>
    public const string Sys_Dic = "Sys_Dic";

    /// <summary>
    /// 菜单
    /// </summary>
    public const string Sys_Menu = "Sys_Menu";

    /// <summary>
    /// 操作日志
    /// </summary>
    public const string Sys_OperationLog = "Sys_OperationLog";

    /// <summary>
    /// 错误日志
    /// </summary>
    public const string Sys_ErrorLog = "Sys_ErrorLog";

    /// <summary>
    /// 租户表
    /// </summary>
    public const string Sys_Tenant = "Sys_Tenant";

    /// <summary>
    /// 机构表
    /// </summary>
    public const string Sys_Org = "Sys_Org";

    /// <summary>
    /// 角色权限表
    /// </summary>
    public const string Sys_RolePermission = "Sys_RolePermission";

    /// <summary>
    /// 程序版本表
    /// </summary>
    public const string Sys_AppVersion = "Sys_AppVersion";

    /// <summary>
    /// 附件表
    /// </summary>
    public const string Sys_Attachment = "Sys_Attachment";

    /// <summary>
    /// 通知信息表
    /// </summary>
    public const string Sys_Notify = "Sys_Notify";

    #endregion
}