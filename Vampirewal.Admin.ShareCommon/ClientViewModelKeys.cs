﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ViewModelKeys
// 创建者：      杨程
// 创建日期：	    2022/12/20 21:13:03

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
///
/// </summary>
public partial class ClientViewModelKeys
{
    /// <summary>
    /// 登陆窗体VM
    /// </summary>
    public const string LoginViewModel = "Vampirewal.Admin.Client.ViewModels.ViewModel.LoginViewModel";

    /// <summary>
    /// 主窗体VM
    /// </summary>
    public const string MainViewModel = "Vampirewal.Admin.Client.ViewModels.ViewModel.MainViewModel";

    /// <summary>
    /// 消息弹窗VM
    /// </summary>
    public const string MessageNotifyViewModel = "Vampirewal.Admin.Client.ViewModels.Controls.MessageNotifyViewModel";

    #region [     系统基础管理(请勿删除)     ]

    #region 用户管理

    /// <summary>
    /// 系统用户管理页VM
    /// </summary>
    public const string SysUserManagerViewModel = "Vampirewal.Admin.Client.ViewModels.ViewModel.SysManager.SysUserManagerViewModel";

    /// <summary>
    /// 系统用户编辑新增页VM
    /// </summary>
    public const string EditSysUserViewModel = "Vampirewal.Admin.Client.ViewModels.ViewModel.SysManager.EditSysUserViewModel";

    #endregion

    #region 角色管理

    /// <summary>
    /// 客户端角色管理列表页
    /// </summary>
    public const string RoleManagerViewModel = "Vampirewal.Admin.Client.ViewModels.ViewModel.SysManager.RoleManagerViewModel";

    #endregion

    #endregion
}