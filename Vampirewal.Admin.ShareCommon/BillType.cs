﻿#region [     文件信息     ]
/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.ShareCommon
 * 唯一标识：f3d95670-b471-4273-8853-8b93e263d368
 * 文件名：BillType
 * 当前用户域：VAMPIREWAL
 * 
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/13 12:41:16
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion [     文件信息     ]

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vampirewal.Admin.ShareCommon;

/// <summary>
/// 业务单据类型
/// </summary>
public enum BillType
{

}
