﻿namespace Vampirewal.Admin.Server.Views.FlowEngineManagerViews;

/// <summary>
/// EditFlowEngineView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.EditFlowEngineView)]
public partial class EditFlowEngineView : AddOrEditUcViewBase
{
    public EditFlowEngineView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.EditFlowEngineViewModel;
}