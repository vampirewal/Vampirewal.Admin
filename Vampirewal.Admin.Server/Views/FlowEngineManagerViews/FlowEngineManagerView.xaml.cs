﻿namespace Vampirewal.Admin.Server.Views.FlowEngineManagerViews;

/// <summary>
/// FlowEngineManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.FlowEngineManagerView)]
public partial class FlowEngineManagerView : LayoutUcViewBase
{
    public FlowEngineManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.FlowEngineManagerViewModel;
}