﻿namespace Vampirewal.Admin.Server.Views.OrganizationManagerViews.Org;

/// <summary>
/// EditOrgView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.EditOrgView)]
public partial class EditOrgView : AddOrEditUcViewBase
{
    public EditOrgView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.EditOrgViewModel;
}