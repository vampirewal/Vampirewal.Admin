﻿namespace Vampirewal.Admin.Server.Views.OrganizationManagerViews.Org;

/// <summary>
/// OrgManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.OrgManagerView)]
public partial class OrgManagerView : LayoutUcViewBase
{
    public OrgManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.OrgManagerViewModel;
}