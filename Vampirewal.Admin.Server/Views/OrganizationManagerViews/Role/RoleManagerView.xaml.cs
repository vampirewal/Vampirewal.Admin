﻿namespace Vampirewal.Admin.Server.Views.OrganizationManagerViews.Role;

/// <summary>
/// RoleManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.RoleManagerView)]
public partial class RoleManagerView : LayoutUcViewBase
{
    public RoleManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.RoleManagerViewModel;
}