﻿namespace Vampirewal.Admin.Server.Views.OrganizationManagerViews.Role;

/// <summary>
/// ConfigurePermissionView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.ConfigurePermissionView)]
public partial class ConfigurePermissionView : AddOrEditUcViewBase
{
    public ConfigurePermissionView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.ConfigurePermissionViewModel;
}