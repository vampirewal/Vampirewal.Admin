﻿namespace Vampirewal.Admin.Server.Views.OrganizationManagerViews.User;

/// <summary>
/// EditUserView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.EditUserView)]
public partial class EditUserView : AddOrEditUcViewBase
{
    public EditUserView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.EditUserViewModel;
}