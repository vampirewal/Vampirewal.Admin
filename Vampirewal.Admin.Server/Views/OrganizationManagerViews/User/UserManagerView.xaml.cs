﻿namespace Vampirewal.Admin.Server.Views.OrganizationManagerViews.User;

/// <summary>
/// UserManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.UserManagerView)]
public partial class UserManagerView : LayoutUcViewBase
{
    public UserManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.UserManagerViewModel;
}