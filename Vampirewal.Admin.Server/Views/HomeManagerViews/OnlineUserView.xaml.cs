﻿namespace Vampirewal.Admin.Server.Views.HomeManagerViews;

/// <summary>
/// OnlineUserView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.OnlineUserView)]
public partial class OnlineUserView : LayoutUcViewBase
{
    public OnlineUserView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.OnlineUserViewModel;
}