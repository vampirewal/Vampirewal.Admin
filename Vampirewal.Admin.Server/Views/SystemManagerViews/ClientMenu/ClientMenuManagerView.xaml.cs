﻿namespace Vampirewal.Admin.Server.Views.SystemManagerViews.ClientMenu;

/// <summary>
/// ClientMenuManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.ClientMenuManagerView)]
public partial class ClientMenuManagerView : LayoutUcViewBase
{
    public ClientMenuManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.ClientMenuManagerViewModel;
}