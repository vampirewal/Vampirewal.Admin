﻿namespace Vampirewal.Admin.Server.Views.SystemManagerViews.ClientMenu;

/// <summary>
/// EditClentMenuView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.EditClientMenuView)]
public partial class EditClentMenuView : AddOrEditUcViewBase
{
    public EditClentMenuView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.EditClientMenuViewModel;
}