﻿namespace Vampirewal.Admin.Server.Views.SystemManagerViews.Tenant;

/// <summary>
/// TenantManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.TenantManagerView)]
public partial class TenantManagerView : LayoutUcViewBase
{
    public TenantManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.TenantManagerViewModel;
}