﻿namespace Vampirewal.Admin.Server.Views.SystemManagerViews.Tenant;

/// <summary>
/// EditTenantView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.EditTenanView)]
public partial class EditTenantView : AddOrEditUcViewBase
{
    public EditTenantView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.EditTenantViewModel;
}