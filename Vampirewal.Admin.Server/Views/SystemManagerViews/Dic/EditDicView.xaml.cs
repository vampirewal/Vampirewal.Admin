﻿namespace Vampirewal.Admin.Server.Views.SystemManagerViews.Dic;

/// <summary>
/// EditDicView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.EditDicView)]
public partial class EditDicView : AddOrEditUcViewBase
{
    public EditDicView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.EditDicViewModel;
}