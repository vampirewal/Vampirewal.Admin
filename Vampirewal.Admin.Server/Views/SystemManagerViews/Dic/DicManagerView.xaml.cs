﻿namespace Vampirewal.Admin.Server.Views.SystemManagerViews.Dic;

/// <summary>
/// DicManagerView.xaml 的交互逻辑
/// </summary>
[VampirewalIoCRegister(ServerViewKeys.DicManagerView)]
public partial class DicManagerView : LayoutUcViewBase
{
    public DicManagerView()
    {
        InitializeComponent();
    }

    public override string ViewModelKey => ServerViewModelKeys.DicManagerViewModel;
}