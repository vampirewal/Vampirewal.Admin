﻿global using System;
global using System.Windows;
global using Vampirewal.Admin.Server.Common;
global using Vampirewal.Admin.Server.ViewModel;
global using Vampirewal.Admin.ShareCommon;
global using Vampirewal.Core.Attributes;
global using Vampirewal.Core.Components;
global using Vampirewal.Core.WPF.Theme;

namespace Vampirewal.Admin.Server;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : VampirewalApplication
{
    protected override void OnStartup(StartupEventArgs e)
    {
        base.OnStartup(e);
    }

    protected override Type BootStartUp()
    {
        return typeof(ServerBootStartUp);
    }
}