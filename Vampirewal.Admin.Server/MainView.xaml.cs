﻿

namespace Vampirewal.Admin.Server
{
    /// <summary>
    /// MainView.xaml 的交互逻辑
    /// </summary>
    [VampirewalIoCRegister(ServerViewKeys.MainView)]
    public partial class MainView : MainWindowBase
    {
        public MainView()
        {
            InitializeComponent();
        }

        public override string ViewModelKey => ServerViewModelKeys.MainViewModel;
    }
}