﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Unit
// 创建者：      杨程
// 创建日期：	    2022/12/17 23:22:54

//----------------------------------------------------------------*/
#endregion


namespace Vampirewal.Admin.Server.Model.Models.FlowEngine;

/// <summary>
/// 流程节点
/// </summary>
[SugarTable("WorkFlow_Unit")]
public class Unit : DetailBaseModel
{
    public Unit()
    {
        //构造函数
    }

    private int _UnitIndex;
    [Description("节点在流程中的索引号，可用于查找节点。在流程中，索引号不能重复。")]
    [DisplayName("索引编号")]
    public int UnitIndex { get => _UnitIndex; set { _UnitIndex = value; OnPropertyChanged(); } }

    private int _UnitType;
    /// <summary>
    /// 节点类型(1是开始，99是结束；2是审核中)
    /// </summary>
    public int UnitType { get => _UnitType; set { _UnitType = value; OnPropertyChanged(); } }

    private string _Name;
    [Description("在流程图上显示的节点名称。")]
    [DisplayName("显示名称")]
    public string Name { get => _Name; set { _Name = value; OnPropertyChanged(); } }

    private bool _IsFirst = false;
    [Description("是否起始节点。一个流程中仅能有且只有一个起始节点。")]
    [DisplayName("是否起始节点")]
    public bool IsFirst { get => _IsFirst; set { _IsFirst = value; OnPropertyChanged(); } }

    private bool _IsEnd = false;
    /// <summary>
    /// 是否终结节点
    /// </summary>
    public bool IsEnd { get => _IsEnd; set { _IsEnd = value; OnPropertyChanged(); } }

    private string _RoleId;
    /// <summary>
    /// 对应角色ID
    /// </summary>
    [SugarColumn(IsNullable = true)]
    public string RoleId { get => _RoleId; set { _RoleId = value; OnPropertyChanged(); } }

    private string _RoleName;
    /// <summary>
    /// 对应角色名称
    /// </summary>
    [SugarColumn(IsNullable = true)]
    public string RoleName { get => _RoleName; set { _RoleName = value; OnPropertyChanged(); } }
}
