﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Work
// 创建者：      杨程
// 创建日期：	    2022/12/17 23:24:32

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Server.Model.Models.FlowEngine
{
    /// <summary>
    /// 工作流程主表
    /// </summary>
    [SugarTable("WorkFlow_Work")]
    public class Work : BillBaseModel
    {
        public Work()
        {
            //构造函数
        }

        /// <summary>
        /// 源单BillId
        /// </summary>
        public string SourceBillId { get; set; }
        /// <summary>
        /// 单据类型
        /// </summary>
        public int BillType { get; set; }

        /// <summary>
        /// 执行的流程ID
        /// </summary>
        public string FlowId { get; set; }

        /// <summary>
        /// 工作流描述
        /// </summary>
        public string WorkDes { get; set; }

        /// <summary>
        /// 开始人ID
        /// </summary>
        public string StartorId { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; } = DateTime.Now;

        [SugarColumn(IsNullable = true, DefaultValue = null)]
        public DateTime FinishTime { get; set; }

        /// <summary>
        /// 工作流状态
        /// </summary>
        public int State { get; set; }

        public string OrgId { get; set; }

        public string OrgName { get; set; }

        /// <summary>
        /// 租户ID
        /// </summary>
        public string TenantId { get; set; }
    }
}
