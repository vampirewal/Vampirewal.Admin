﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Link
// 创建者：      杨程
// 创建日期：	    2022/12/17 23:21:22

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Server.Model.Models.FlowEngine;

/// <summary>
/// 流程连线
/// </summary>
[SugarTable("WorkFlow_Link")]
public class Link : DetailBaseModel
{
    public Link()
    {
        //构造函数
    }

    [DisplayName("条件")]
    [SugarColumn(IsNullable = true, ColumnDescription = "条件")]
    public string Condition { get; set; }

    [DisplayName("显示名称")]
    [SugarColumn(IsNullable = true, ColumnDescription = "显示名称")]
    public string DisplayText { get; set; }

    /// <summary>
    /// 上级节点ID
    /// </summary>
    public string UpUnitId { get; set; }

    /// <summary>
    /// 下级节点ID
    /// </summary>
    public string NextUnitId { get; set; }
}
