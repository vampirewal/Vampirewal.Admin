﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Sys_RolePermission
// 创建者：      杨程
// 创建日期：	    2022/12/24 22:39:23

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Server.Model.Models.Systems.ClientMenu
{
    /// <summary>
    /// 角色权限表
    /// </summary>
    [SugarTable(SugarTableKeys.Sys_RolePermission)]
    public partial class Sys_RolePermission : BillBaseModel
    {


        private string _RoleId;
        /// <summary>
        /// 角色ID
        /// </summary>
        public string RoleId
        {
            get
            {
                return _RoleId;
            }
            set
            {
                _RoleId = value;
                OnPropertyChanged();
            }
        }






        private string _PermissionId;
        /// <summary>
        /// 菜单按钮权限ID
        /// </summary>
        public string PermissionId
        {
            get
            {
                return _PermissionId;
            }
            set
            {
                _PermissionId = value;
                OnPropertyChanged();
            }
        }


    }
}
