﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Sys_RoleTemplate
// 创建者：      杨程
// 创建日期：	    2022/12/20 15:09:35

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Server.Model.Models.Systems.Role
{
    /// <summary>
    /// 角色模版
    /// </summary>
    [SugarTable("Sys_RoleTemplate")]
    public class Sys_RoleTemplate : BillBaseModel
    {
        private string _RoleName;
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; OnPropertyChanged(); }
        }



        private string _Code;
        /// <summary>
        /// 编码
        /// </summary>
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                _Code = value;
                OnPropertyChanged();
            }
        }


        private bool _IsActive = true;

        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; OnPropertyChanged(); }
        }



        private bool _IsAdmin=false;
        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool IsAdmin
        {
            get
            {
                return _IsAdmin;
            }
            set
            {
                _IsAdmin = value;
                OnPropertyChanged();
            }
        }



        private string _TenantId;
        /// <summary>
        /// 租户ID
        /// </summary>
        [SugarColumn(ColumnDescription = "租户ID")]
        public string TenantId { get => _TenantId; set { _TenantId = value; OnPropertyChanged(); } }



        private int _DataScope;
        /// <summary>
        /// 角色数据权限(0全部，1是当前机构及下级机构，2是当前机构，3是无自能看自己)
        /// </summary>
        public int DataScope
        {
            get
            {
                return _DataScope;
            }
            set
            {
                _DataScope = value;
                OnPropertyChanged();
            }
        }

    }
}
