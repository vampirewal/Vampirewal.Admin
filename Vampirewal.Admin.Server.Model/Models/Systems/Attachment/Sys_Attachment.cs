﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Sys_Attachment
// 创建者：      杨程
// 创建日期：	    2023/1/9 20:00:33

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Server.Model.Models.Systems.Attachment;

/// <summary>
/// 附件表(BillId存各业务线Billid)
/// </summary>
[SugarTable(SugarTableKeys.Sys_Attachment)]
public partial class Sys_Attachment:DetailBaseModel
{
    /// <summary>
    /// 文件名
    /// </summary>
    public string FileName { get; set; }

    /// <summary>
    /// 文件保存路径
    /// </summary>
    public string FilePath { get; set; }

    /// <summary>
    /// 文件大小
    /// </summary>
    public string FileSize { get; set; }

    /// <summary>
    /// 上传人ID
    /// </summary>
    public string UploaderId { get; set; }

    /// <summary>
    /// 上传人
    /// </summary>
    public string Uploader { get; set; }

    /// <summary>
    /// 上传时间
    /// </summary>
    public DateTime UploadTime { get; set; }

    /// <summary>
    /// 在服务器上的名称
    /// </summary>
    public string ServerName { get; set; } = "";
}
