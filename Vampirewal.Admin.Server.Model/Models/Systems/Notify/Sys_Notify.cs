﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Sys_Notify
// 创建者：      杨程
// 创建日期：	    2023/1/19 10:22:55

//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Vampirewal.Admin.Server.Model.Models.Systems.Notify;

/// <summary>
/// 储存通知信息
/// </summary>
[SugarTable(SugarTableKeys.Sys_Notify,tableDescription: "储存通知信息")]
public partial class Sys_Notify:BillBaseModel
{
    /// <summary>
    /// 通知类型(0是一般通知，1是成功通知，2是错误通知,3是临时通知)
    /// </summary>
    [SugarColumn(ColumnDescription = "通知类型(0是一般通知，1是成功弹窗通知，2是错误弹窗通知,3是临时弹窗通知)")]
    public int NotifyType { get; set; }

    /// <summary>
    /// 通知内容
    /// </summary>
    [SugarColumn(ColumnDescription = "通知内容")]
    public string NotifyMsg { get; set; }

    /// <summary>
    /// 是否已接收
    /// </summary>
    [SugarColumn(ColumnDescription = "是否已接收")]
    public bool IsReceived { get; set; }

    /// <summary>
    /// 接收时间
    /// </summary>
    [SugarColumn(ColumnDescription = "接收时间",IsNullable =true)]
    public DateTime? ReceivedTime { get; set; }

    /// <summary>
    /// 发送人ID
    /// </summary>
    [SugarColumn(ColumnDescription = "发送人ID")]
    public string SendUserId { get; set; }

    /// <summary>
    /// 接收人ID
    /// </summary>
    [SugarColumn(ColumnDescription = "接收人ID")]
    public string ReceiveUserId { get; set; }

    /// <summary>
    /// 机构ID
    /// </summary>
    [SugarColumn(ColumnDescription = "机构ID")]
    public string OrgId { get; set; }

    /// <summary>
    /// 租户ID
    /// </summary>
    [SugarColumn(ColumnDescription = "租户ID")]
    public string TenantId { get; set; }


}
