﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    GlobalUsing
// 创建者：      杨程
// 创建日期：	    2022/12/15 13:38:18

//----------------------------------------------------------------*/
#endregion

global using SqlSugar;
global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Text;
global using System.Threading.Tasks;
global using Vampirewal.Admin.Server.Common;
global using CommunityToolkit.Mvvm.ComponentModel;
global using Vampirewal.Core.Models;
global using Vampirewal.Core.Attributes;
global using Vampirewal.Core.WpfTheme.CustomControl;
global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using Vampirewal.Core;
global using System.Reflection;
global using Vampirewal.Admin.Server.Model.Models.Systems.User;