﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Flow
// 创建者：      杨程
// 创建日期：	    2022/12/17 23:16:28

//----------------------------------------------------------------*/
#endregion



namespace Vampirewal.Admin.ShareModel.FlowEngine;

public enum FlowState
{
    [SetClassification(Type = 0)]
    [Display(Name = "停用")]
    fsNotUse,
    [SetClassification(Type = 1)]
    [Display(Name = "正常")]
    fsNormal
}

public enum LoadFrom { File = 0, Copy = 1, Database = 2 }
/// <summary>
/// 
/// </summary>
[SugarTable("WorkFlow_Flow")]
public class Flow : BillBaseModel
{
    public Flow()
    {
        this.BillType = 0;
        this.Description = "新流程";
        this.FlowCode = "";
        //Units = new StateList<Unit>();
    }

    private string _FlowCode;
    /// <summary>
    /// 流程编码
    /// </summary>
    [SugarColumn(ColumnDescription = "流程编码")]
    [Description("流程的编码，最好使用名空间的格式，例如 Eteam.Fu2.SalesOrder")]
    public string FlowCode { get => _FlowCode; set { _FlowCode = value; OnPropertyChanged(); } }

    private string _Description;
    /// <summary>
    /// 流程名称
    /// </summary>
    [Description("流程的描述，会以标题方式显示")]
    [DisplayName("流程名称")]
    public string Description { get => _Description; set { _Description = value; OnPropertyChanged(); } }

    private int _BillType;
    /// <summary>
    /// 表单类型
    /// </summary>
    [DisplayName("单据名称")]
    [Description("流程属于哪一种单据")]
    public int BillType { get => _BillType; set { _BillType = value; OnPropertyChanged(); } }

    private int _BillClass;
    /// <summary>
    /// 单据所属子系统
    /// </summary>
    [DisplayName("单据所属子系统")]
    public int BillClass { get => _BillClass; set { _BillClass = value; OnPropertyChanged(); } }

    private string _OrgId;
    /// <summary>
    /// 对应
    /// </summary>
    public string OrgId { get => _OrgId; set { _OrgId = value; OnPropertyChanged(); } }
    

    private int _State;
    [Description("流程的状态")]
    [DisplayName("状态")]
    public int State { get => _State; set { _State = value; OnPropertyChanged(); } }

    public string StateStr
    {
        get
        {
            return ((FlowState)State).GetDisplay();
        }
    }

    private int _Version;
    /// <summary>
    /// 版本号
    /// </summary>
    [DisplayName("版本号")]
    [Description("流程的版本号")]
    [ReadOnly(true)]
    public int Version { get => _Version; set { _Version = value; OnPropertyChanged(); } }

    private string _TenantId;
    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId { get => _TenantId; set { _TenantId = value; OnPropertyChanged(); } }
}
