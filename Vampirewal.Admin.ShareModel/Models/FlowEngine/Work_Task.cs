﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    Work_Task
// 创建者：      杨程
// 创建日期：	    2022/12/17 23:23:53

//----------------------------------------------------------------*/
#endregion




namespace Vampirewal.Admin.ShareModel.FlowEngine;

public enum TaskState
{
    tsReady,
    tsTodo,
    tsComplete,
    tsError,
    tsCancel
}
/// <summary>
/// 流程任务执行
/// </summary>
[SugarTable("WorkFlow_Task")]
public class Work_Task : DetailBaseModel
{
    public Work_Task()
    {
        //构造函数
    }

    /// <summary>
    /// 节点ID
    /// </summary>
    public string UnitId { get; set; }

    public TaskState State { get; set; }

    public string Description { get; set; }

    [SugarColumn(IsNullable = true)]
    public DateTime NotifyTime { get; set; }

    [SugarColumn(IsNullable = true, DefaultValue = null)]
    public DateTime FinishTime { get; set; }

    /// <summary>
    /// 执行人
    /// </summary>
    public string Executor { get; set; }

    /// <summary>
    /// 是否审核通过
    /// </summary>
    public bool IsSuccess { get; set; } = false;

    /// <summary>
    /// 审核意见
    /// </summary>
    [SugarColumn(IsNullable = true)]
    public string Opinion { get; set; }

    /// <summary>
    /// 单据类型
    /// </summary>
    public int BillType { get; set; }
}
