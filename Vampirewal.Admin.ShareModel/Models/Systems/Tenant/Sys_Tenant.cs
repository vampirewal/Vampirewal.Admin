﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_Tenant
// 创建者：      杨程
// 创建日期：	    2022/12/16 13:26:49

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareModel.Models.Systems.Tenant;

/// <summary>
/// 租户表
/// </summary>
[GeneratorDTO]
[SugarTable(SugarTableKeys.Sys_Tenant)]
public partial class Sys_Tenant : BillBaseModel
{
    private string _TenantName;

    /// <summary>
    /// 租户名称
    /// </summary>
    public string TenantName
    {
        get
        {
            return _TenantName;
        }
        set
        {
            _TenantName = value;
            OnPropertyChanged();
        }
    }

    private string _Code;

    /// <summary>
    /// 租户编码
    /// </summary>
    public string Code
    {
        get
        {
            return _Code;
        }
        set
        {
            _Code = value;
            OnPropertyChanged();
        }
    }

    private string _PhoneNum;

    /// <summary>
    /// 联系方式
    /// </summary>
    public string PhoneNum
    {
        get
        {
            return _PhoneNum;
        }
        set
        {
            _PhoneNum = value;
            OnPropertyChanged();
        }
    }

    private string _AdminEmailAddress;

    /// <summary>
    /// 管理员邮箱地址
    /// </summary>
    public string AdminEmailAddress
    {
        get
        {
            return _AdminEmailAddress;
        }
        set
        {
            _AdminEmailAddress = value;
            OnPropertyChanged();
        }
    }

    private string _AdminPassWord;

    /// <summary>
    /// 管理员密码
    /// </summary>
    public string AdminPassWord
    {
        get
        {
            return _AdminPassWord;
        }
        set
        {
            _AdminPassWord = value;
            OnPropertyChanged();
        }
    }

    private DateTime _EndTime = DateTime.Now;

    /// <summary>
    /// 到期时间(默认7天后到期)
    /// </summary>
    public DateTime EndTime
    {
        get
        {
            return _EndTime;
        }
        set
        {
            _EndTime = value;
            OnPropertyChanged();
        }
    }

    private string _Remark;

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark
    {
        get
        {
            return _Remark;
        }
        set
        {
            _Remark = value;
            OnPropertyChanged();
        }
    }

    private bool _IsActive;

    /// <summary>
    /// 是否启用
    /// </summary>
    public bool IsActive
    {
        get
        {
            return _IsActive;
        }
        set
        {
            _IsActive = value;
            OnPropertyChanged();
        }
    }
}