﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_Log
// 创建者：      杨程
// 创建日期：	    2022/12/16 12:41:01

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareModel.Models.Systems.CustomLog;

/// <summary>
/// 操作日志
/// </summary>
[SugarTable(SugarTableKeys.Sys_OperationLog)]
public partial class Sys_OperationLog : Logger
{
    /// <summary>
    /// 租户下机构ID
    /// </summary>
    public string OrgId { get; set; }

    /// <summary>
    /// 租户下机构名称
    /// </summary>
    public string OrgName { get; set; }

    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId { get; set; }

    [SugarColumn(IsIgnore = true)]
    public new string UpdateBy { get; set; }

    [SugarColumn(IsIgnore = true)]
    public new string UpdateTime { get; set; }

    [SugarColumn(IsIgnore = true)]
    public new string UpdateUserId { get; set; }
}

/// <summary>
/// 操作日志
/// </summary>
[SugarTable(SugarTableKeys.Sys_ErrorLog)]
public partial class Sys_ErrorLog : Logger
{
    /// <summary>
    /// 租户下机构ID
    /// </summary>
    public string OrgId { get; set; }

    /// <summary>
    /// 租户下机构名称
    /// </summary>
    public string OrgName { get; set; }

    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId { get; set; }

    [SugarColumn(IsIgnore = true)]
    public new string UpdateBy { get; set; }

    [SugarColumn(IsIgnore = true)]
    public new string UpdateTime { get; set; }

    [SugarColumn(IsIgnore = true)]
    public new string UpdateUserId { get; set; }
}