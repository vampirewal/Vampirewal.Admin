﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_Org
// 创建者：      杨程
// 创建日期：	    2022/12/16 13:28:36

//----------------------------------------------------------------*/

#endregion





namespace Vampirewal.Admin.ShareModel.Models.Systems.Org;

/// <summary>
/// 机构表
/// </summary>
[GeneratorDTO]
[SugarTable(SugarTableKeys.Sys_Org)]
public partial class Sys_Org : BillBaseModel
{
    private string _OrgName;

    /// <summary>
    /// 机构名称
    /// </summary>
    public string OrgName
    {
        get { return _OrgName; }
        set { _OrgName = value; OnPropertyChanged(); }
    }

    private int _orgtype = 1;

    /// <summary>
    /// 机构类型(0是系统，1是租户机构，2是部门)
    /// </summary>
    [SugarColumn(ColumnName = "OrgType")]
    public int OrgType
    {
        get { return _orgtype; }
        set { _orgtype = value; OnPropertyChanged(); }
    }

    [DtoIgnore]
    [SugarColumn(IsIgnore = true)]
    public string OrgTypeStr
    {
        get
        {
            switch (OrgType)
            {
                case 0:
                    return "系统";

                case 1:
                    return "租户机构";

                case 2:
                    return "部门";

                default:
                    return "";
            }
        }
    }

    private bool _State = true;

    /// <summary>
    /// 状态（0停用1启用）
    /// </summary>
    public bool State
    {
        get { return _State; }
        set { _State = value; OnPropertyChanged(); }
    }

    private string _TenantId;

    /// <summary>
    /// 租户ID
    /// </summary>
    public string TenantId
    { get => _TenantId; set { _TenantId = value; OnPropertyChanged(); } }

    private string _ListId;

    /// <summary>
    /// 当前层级ID
    /// </summary>
    public string ListId
    {
        get
        {
            return _ListId;
        }
        set
        {
            _ListId = value;
            OnPropertyChanged();
        }
    }

    private string _ParentId;

    /// <summary>
    /// 父ID
    /// </summary>
    public string ParentId
    {
        get { return _ParentId; }
        set { _ParentId = value; OnPropertyChanged(); }
    }
}

public partial class Sys_Org : ITreeNode
{
    public object GetCurrentNodeMark()
    {
        return ListId;
    }

    public object GetParentNodeMark()
    {
        return ParentId;
    }

    public bool IsParentNode(ITreeNode parentNode)
    {
        if (parentNode is Sys_Org node)
            return ParentId == node.ListId;
        return false;
    }

    public bool IsSameNode(ITreeNode node)
    {
        if (node is Sys_Org tmpNode)
            return ListId == tmpNode.ListId;
        return false;
    }

    public bool IsSameParent(ITreeNode node)
    {
        if (node is Sys_Org tmpNode)
            return ParentId == tmpNode.ParentId;
        return false;
    }

    public bool IsSubNode(ITreeNode subNode)
    {
        if (subNode is Sys_Org tmpNode)
            return tmpNode.ParentId == ListId;
        return false;
    }

    public void SetParentNodeMark(object parentObj)
    {
        ParentId = parentObj.ToString();
    }
}