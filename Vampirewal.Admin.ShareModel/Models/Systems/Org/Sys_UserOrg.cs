﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_UserOrg
// 创建者：      杨程
// 创建日期：	    2022/12/20 15:24:59

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareModel.Models.Systems.Org;

/// <summary>
/// 用户机构中间表
/// </summary>
[GeneratorDTO]
[SugarTable("Sys_UserOrg")]
public partial class Sys_UserOrg : BillBaseModel
{
    private string _UserId;

    /// <summary>
    ///
    /// </summary>
    public string UserId
    {
        get { return _UserId; }
        set { _UserId = value; OnPropertyChanged(); }
    }

    private string _OrgId;

    /// <summary>
    ///
    /// </summary>
    public string OrgId
    {
        get { return _OrgId; }
        set { _OrgId = value; OnPropertyChanged(); }
    }

    private string _Position;

    /// <summary>
    /// 岗位
    /// </summary>
    public string Position
    {
        get { return _Position; }
        set { _Position = value; OnPropertyChanged(); }
    }
}