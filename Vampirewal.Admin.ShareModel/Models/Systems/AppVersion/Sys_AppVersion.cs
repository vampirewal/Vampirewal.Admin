﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_AppVersion
// 创建者：      杨程
// 创建日期：	    2023/1/9 19:58:19

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareModel.Models.Systems.AppVersion;

/// <summary>
/// 程序版本
/// </summary>
[GeneratorDTO]
[SugarTable(SugarTableKeys.Sys_AppVersion)]
public partial class Sys_AppVersion : BillBaseModel
{
    /// <summary>
    /// 版本号1
    /// </summary>
    public string Version { get; set; }

    /// <summary>
    /// 描述
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// 发布时间
    /// </summary>
    public DateTime PublishTime { get; set; }
}