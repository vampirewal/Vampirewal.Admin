﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_UserRole
// 创建者：      杨程
// 创建日期：	    2022/12/20 15:23:10

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareModel.Models.Systems.Role;

/// <summary>
/// 用户角色关联表
/// </summary>
[GeneratorDTO]
[SugarTable("Sys_UserRole")]
public partial class Sys_UserRole : BillBaseModel
{
    private string _UserId;

    public string UserId
    {
        get { return _UserId; }
        set { _UserId = value; OnPropertyChanged(); }
    }

    private string _RoleId;

    public string RoleId
    {
        get { return _RoleId; }
        set { _RoleId = value; OnPropertyChanged(); }
    }
}