﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    Sys_Dic
// 创建者：      杨程
// 创建日期：	    2022/12/15 13:48:05

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareModel.Models.Systems.Dic;

/// <summary>
/// 字典值
/// </summary>
[GeneratorDTO]
[SugarTable(SugarTableKeys.Sys_Dic)]
public partial class Sys_Dic : DetailBaseModel
{
    private int _DicValue = 0;

    /// <summary>
    /// 字典值
    /// </summary>
    [SugarColumn(IsNullable = false, ColumnDescription = "字典值")]
    [ListenDetailChange]
    public int DicValue
    {
        get
        {
            return _DicValue;
        }
        set
        {
            _DicValue = value;
            OnPropertyChanged();
        }
    }

    private string _Description;

    /// <summary>
    /// 字典描述
    /// </summary>
    [SugarColumn(IsNullable = false, ColumnDescription = "字典描述")]
    [ListenDetailChange]
    public string Description
    {
        get
        {
            return _Description;
        }
        set
        {
            _Description = value;
            OnPropertyChanged();
        }
    }

    private bool _IsActive = true;

    /// <summary>
    /// 是否启用
    /// </summary>
    [SugarColumn(IsNullable = false, ColumnDescription = "是否启用")]
    [ListenDetailChange]
    public bool IsActive
    {
        get
        {
            return _IsActive;
        }
        set
        {
            _IsActive = value;
            OnPropertyChanged();
        }
    }
}

public partial class Sys_DicVO : DetailItemChangeBaseModel<Sys_Dic>
{
    public Sys_DicVO(Sys_Dic dto) : base(dto)
    {
    }

    private int _DicValue = 0;

    /// <summary>
    /// 字典值
    /// </summary>
    [ListenDetailChange]
    public int DicValue
    {
        get
        {
            return _DicValue;
        }
        set
        {
            _DicValue = value;
            OnPropertyChanged();
        }
    }

    private string _Description;

    /// <summary>
    /// 字典描述
    /// </summary>
    [ListenDetailChange]
    public string Description
    {
        get
        {
            return _Description;
        }
        set
        {
            _Description = value;
            OnPropertyChanged();
        }
    }

    private bool _IsActive = true;

    /// <summary>
    /// 是否启用
    /// </summary>
    [ListenDetailChange]
    public bool IsActive
    {
        get
        {
            return _IsActive;
        }
        set
        {
            _IsActive = value;
            OnPropertyChanged();
        }
    }

    public override void LoadDTO(Sys_Dic dto)
    {
        this.DtlId = dto.DtlId;
        this.BillId = dto.BillId;
        this.DicValue = dto.DicValue;
        this.Description = dto.Description;
        this.IsActive = dto.IsActive;
    }

    public Sys_Dic ToDto()
    {
        Sys_Dic dic = new Sys_Dic()
        {
            BillId = this.BillId,
            Description = this.Description,
            DicValue = this.DicValue,
            IsActive = this.IsActive,
        };

        return dic;
    }
}