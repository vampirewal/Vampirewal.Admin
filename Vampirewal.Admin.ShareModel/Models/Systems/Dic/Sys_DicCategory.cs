﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    DicCategory
// 创建者：      杨程
// 创建日期：	    2022/12/15 13:35:18

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.ShareModel.Models.Systems.Dic;

/// <summary>
/// 字典类别
/// </summary>
[GeneratorDTO]
[SugarTable(SugarTableKeys.Sys_DicCategory)]
public partial class Sys_DicCategory : BillBaseModel
{
    private string _DicCategoryName;

    /// <summary>
    /// 类别名称
    /// </summary>
    [SugarColumn(IsNullable = false, ColumnDescription = "类别名称")]
    public string DicCategoryName
    {
        get
        {
            return _DicCategoryName;
        }
        set
        {
            _DicCategoryName = value;
            OnPropertyChanged();
        }
    }

    private bool _IsActive = true;

    /// <summary>
    /// 是否启用
    /// </summary>
    [SugarColumn(IsNullable = false, ColumnDescription = "是否启用")]
    public bool IsActive
    {
        get
        {
            return _IsActive;
        }
        set
        {
            _IsActive = value;
            OnPropertyChanged();
        }
    }

    private bool _IsPublic = false;

    /// <summary>
    /// 是否公共使用
    /// </summary>
    public bool IsPublic
    {
        get
        {
            return _IsPublic;
        }
        set
        {
            _IsPublic = value;
            OnPropertyChanged();
        }
    }

    private string _TenantId;

    /// <summary>
    /// 租户ID
    /// </summary>
    [SugarColumn(IsNullable = true)]
    public string TenantId
    { get => _TenantId; set { _TenantId = value; OnPropertyChanged(); } }
}

public partial class Sys_DicCategory
{
    private List<Sys_Dic> _DicList;
    /// <summary>
    ///
    /// </summary>

    [SugarColumn(IsIgnore = true)]
    public List<Sys_Dic> DicList
    {
        get
        {
            return _DicList;
        }
        set
        {
            _DicList = value;
            OnPropertyChanged();
        }
    }
}