﻿// MIT 许可证
//
// 版权 © 2020-present Vampirewal
//
// 特此免费授予任何获得本软件副本和相关文档文件（下称“软件”）的人不受限制地处置该软件的权利，
// 包括不受限制地使用、复制、修改、合并、发布、分发、转授许可和/或出售该软件副本，
// 以及再授权被配发了本软件的人如上的权利，须在下列条件下：
//
// 上述版权声明和本许可声明应包含在该软件的所有副本或实质成分中。
//
// 本软件是“如此”提供的，没有任何形式的明示或暗示的保证，包括但不限于对适销性、特定用途的适用性和不侵权的保证。
// 在任何情况下，作者或版权持有人都不对任何索赔、损害或其他责任负责，无论这些追责来自合同、侵权或其它行为中，
// 还是产生于、源于或有关于本软件以及本软件的使用或其它处置。

#region [     文件信息     ]
/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.ShareModel
 * 唯一标识：ff41ad27-3828-4003-9066-fa9a12a90c92
 * 文件名：Class1
 * 当前用户域：VAMPIREWAL
 * 
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/5/19 13:04:14
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion [     文件信息     ]

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vampirewal.Admin.ShareModel
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Class1
    {
        public Class1()
        {
            //构造函数
        }

        #region [     属性     ]

        #endregion

        #region [     公开方法     ]

        #endregion

        #region [     私有方法     ]

        #endregion

        #region [     Command命令     ]

        #endregion

    }
}
