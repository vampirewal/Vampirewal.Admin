﻿
#region 文件信息
/*----------------------------------------------------------------
// 
// 文件名称：	对应服务端的Rpc方法
// 文件功能描述：	
// 设计要求：	
//
// 文 件 名：    RpcMethod
// 创建者：      Vampirewal
// 创建日期：	    ${CreateTime}

//----------------------------------------------------------------*/
#endregion


namespace Vampirewal.Admin.ShareCommon;

/// <summary>
/// RPC接口方法地址
/// </summary>
public sealed class RpcMethod
{
${context}
}
