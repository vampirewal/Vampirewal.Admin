﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ActionFilterAttribute
// 创建者：      杨程
// 创建日期：	    2022/12/16 11:19:40

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.Filters;

/// <summary>
/// 方法执行条件
/// </summary>
public class ActionFilterAttribute : Attribute, IRpcActionFilter
{
    public void Executed(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
    }

    public Task ExecutedAsync(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
        return Task.CompletedTask;
    }

    public void ExecutException(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult, Exception exception)
    {
        invokeResult.Message = exception.ToString();
    }

    public Task ExecutExceptionAsync(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult, Exception exception)
    {
        return Task.CompletedTask;
    }

    public void Executing(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
        if (callContext.CheckTcpClient())
        {
            try
            {
                var user = callContext.GetClientUser();
                if (user != null)
                    throw new Exception("不是正确的用户调用接口！");

                invokeResult.Status = InvokeStatus.Ready;
            }
            catch (Exception ex)
            {
                invokeResult.Status = InvokeStatus.Exception;
                throw ex;
            }
        }
        else
        {
            invokeResult.Status = InvokeStatus.InvocationException;
            throw new Exception("暂时不支持除TCP以外的方式调用接口！");
        }
    }

    public Task ExecutingAsync(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
        return Task.CompletedTask;
    }
}