﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    LogWriteAttribute
// 创建者：      杨程
// 创建日期：	    2022/12/16 11:34:13

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.Filters;

/// <summary>
/// 日志记录
/// </summary>
public class LogWriteAttribute : Attribute, IRpcActionFilter
{
    public void Executed(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
        callContext.CheckCaller(out Sys_User user);

        var OperationLog = VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<Sys_OperationLog>>();

        Sys_OperationLog log = new Sys_OperationLog()
        {
            BillId = Guid.NewGuid().ToString(),
            SystemMoudleName = callContext.MethodInstance.ServerType.Name,
            CreateBy = user.Name,
            CreateUserId = user.BillId,
            CreateTime = DateTime.Now,
            LoggerContent = $"用户:{user.Name}执行了'{callContext.MethodInstance.Name}'方法",
            loggerType = Core.Models.LoggerType.Debug,
            OrgId = user.OrgId,
            OrgName = user.OrgName,
            TenantId = user.TenantId,
        };

        OperationLog.Insert(log);
    }

    public Task ExecutedAsync(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
        return Task.CompletedTask;
    }

    public void ExecutException(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult, Exception exception)
    {
        invokeResult.Message = exception.ToString();

        if (callContext.Caller is HttpTouchRpcSocketClient client)
        {
            try
            {
                var ErrorLog = VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<Sys_ErrorLog>>();

                var user = JsonConvert.DeserializeObject<Sys_User>(client.ID);

                Sys_ErrorLog log = new Sys_ErrorLog()
                {
                    BillId = Guid.NewGuid().ToString(),
                    SystemMoudleName = callContext.MethodInstance.ServerType.Name,
                    CreateBy = user.Name,
                    CreateUserId = user.BillId,
                    CreateTime = DateTime.Now,
                    LoggerContent = $"用户:{user.Name}执行了'{callContext.MethodInstance.Name}'方法异常,报错信息:{exception}",
                    loggerType = Core.Models.LoggerType.Error,
                    OrgId = user.OrgId,
                    OrgName = user.OrgName,
                    TenantId = user.TenantId,
                };

                ErrorLog.AsInsertable(log).ExecuteCommand();

                //ErrorLog.Insert(log);
            }
            catch
            {
            }
        }
    }

    public Task ExecutExceptionAsync(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult, Exception exception)
    {
        invokeResult.Message = exception.ToString();

        return Task.CompletedTask;
    }

    public void Executing(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
    }

    public Task ExecutingAsync(ICallContext callContext, object[] paramter, ref InvokeResult invokeResult)
    {
        return Task.CompletedTask;
    }
}