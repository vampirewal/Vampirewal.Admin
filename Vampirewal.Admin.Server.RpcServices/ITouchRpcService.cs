﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ITouchRpcService
// 创建者：      杨程
// 创建日期：	    2023/1/6 19:14:25

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices;

/// <summary>
/// TouchSocket服务
/// </summary>
public interface ITouchRpcService
{
    /// <summary>
    /// 启动RPC
    /// </summary>
    void Start();

    /// <summary>
    /// 停止RPC
    /// </summary>
    void Stop();

    /// <summary>
    /// 当前启动状态
    /// </summary>
    bool IsStart { get; set; }
}