﻿#region [     文件信息     ]

/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.Server.RpcServices.RpcPlugin
 * 唯一标识：d521d242-bc0b-4b7e-a5b2-301857b7cf6b
 * 文件名：FileOperationPlugin
 * 当前用户域：VAMPIREWAL
 *
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/14 17:30:20
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion [     文件信息     ]

namespace Vampirewal.Admin.Server.RpcServices.RpcPlugin;

/// <summary>
/// Touchsocket文件操作插件
/// </summary>
public partial class FileOperationPlugin : TouchRpcPluginBase<TcpTouchRpcSocketClient>
{
    protected override void OnFileTransfering(TcpTouchRpcSocketClient client, FileOperationEventArgs e)
    {
        e.IsPermitOperation = true;//运行操作

        string? operation = e.Metadata["Operation"];

        if (!string.IsNullOrEmpty(operation))
        {
            if (operation == "上传附件")
            {
                var user = ConverUser(client.ID);
            }
            else if (operation == "下载附件")
            {
                var user = ConverUser(client.ID);
            }
            else if (operation == "发布新版本")
            {
            }
            else if (operation == "下载新版本")
            {
            }
        }

        //有可能是上传，也有可能是下载
        //client.Logger.Info($"有客户端请求传输文件，ID={client.ID}，请求类型={e.TransferType}，请求文件名={e.ResourcePath}");
    }

    protected override void OnFileTransfered(TcpTouchRpcSocketClient client, FileTransferStatusEventArgs e)
    {
        //传输结束，但是不一定成功，需要从e.Result判断状态。
        client.Logger.Info($"客户端传输文件结束，ID={client.ID}，请求类型={e.TransferType}，文件名={e.ResourcePath}，请求状态={e.Result}");
    }

    protected override void OnHandshaked(TcpTouchRpcSocketClient client, VerifyOptionEventArgs e)
    {
        client.Logger.Info($"有客户端成功验证，ID={client.ID}");
    }

    protected override void OnDisconnected(TcpTouchRpcSocketClient client, DisconnectEventArgs e)
    {
        client.Logger.Info($"有客户端断开，ID={client.ID}");
        base.OnDisconnected(client, e);
    }

    protected override void OnRouting(TcpTouchRpcSocketClient client, PackageRouterEventArgs e)
    {
        if (e.RouterType == RouteType.PushFile || e.RouterType == RouteType.PullFile || e.RouterType == RouteType.CreateChannel)
        {
            e.IsPermitOperation = true;
        }
        base.OnRouting(client, e);
    }

    protected override Task OnFileTransferingAsync(TcpTouchRpcSocketClient client, FileOperationEventArgs e)
    {
        e.IsPermitOperation = true;//运行操作

        string? operation = e.Metadata["Operation"];

        if (!string.IsNullOrEmpty(operation))
        {
            if (operation == "上传附件")
            {
                var user = ConverUser(client.ID);
            }
            else if (operation == "下载附件")
            {
                var user = ConverUser(client.ID);
            }
            else if (operation == "发布新版本")
            {
            }
            else if (operation == "下载新版本")
            {
            }
        }

        return base.OnFileTransferingAsync(client, e);
    }

    protected override Task OnFileTransferedAsync(TcpTouchRpcSocketClient client, FileTransferStatusEventArgs e)
    {
        return base.OnFileTransferedAsync(client, e);
    }

    protected override Task OnRoutingAsync(TcpTouchRpcSocketClient client, PackageRouterEventArgs e)
    {
        if (e.RouterType == RouteType.PushFile || e.RouterType == RouteType.PullFile || e.RouterType == RouteType.CreateChannel)
        {
            e.IsPermitOperation = true;
        }

        return base.OnRoutingAsync(client, e);
    }

    private Sys_User ConverUser(string id)
    {
        return JsonConvert.DeserializeObject<Sys_User>(id);
    }
}