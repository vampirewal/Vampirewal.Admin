﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ClientMutualCallPlugin
// 创建者：      杨程
// 创建日期：	    2023/1/18 12:44:52

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.RpcPlugin;

/// <summary>
/// 允许客户端相互调用插件
/// </summary>
internal partial class ClientMutualCallPlugin : TouchRpcPluginBase
{
    protected override void OnRouting(ITouchRpc client, PackageRouterEventArgs e)
    {
        if (e.RouterType == RouteType.Rpc || e.RouterType == RouteType.PushFile || e.RouterType == RouteType.PullFile || e.RouterType == RouteType.CreateChannel)
        {
            e.IsPermitOperation = true;
        }
        base.OnRouting(client, e);
    }
}