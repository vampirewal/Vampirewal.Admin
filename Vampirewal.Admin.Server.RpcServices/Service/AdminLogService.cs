﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    AdminLogService
// 创建者：      杨程
// 创建日期：	    2022/12/16 12:56:49

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// admin日志服务
/// </summary>
public partial class AdminLogService : IVampirewalCoreLogService
{
    /// <summary>
    ///
    /// </summary>
    public AdminLogService()
    {
        //构造函数
    }

    public void DebugLog(string Msg, string SystemModuleName)
    {
    }

    public void ErrorLog(string Msg, string SystemModuleName)
    {
    }

    public void SQLLog(string Msg, string SystemModuleName)
    {
    }

    public void WarningLog(string Msg, string SystemModuleName)
    {
    }

    public void WriteLog(string Msg, LoggerType loggerType, string SystemModuleName)
    {
        var rep = VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<Sys_OperationLog>>();

        Sys_OperationLog logger = new Sys_OperationLog()
        {
            LoggerContent = Msg,
            loggerType = loggerType,
            SystemMoudleName = SystemModuleName,
            CreateTime = DateTime.Now,
            CreateBy = "",
            CreateUserId = "",
            OrgId = "",
            OrgName = "",
            TenantId = "",
        };

        rep.Insert(logger);
    }
}