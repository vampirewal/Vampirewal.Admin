﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    SystemService
// 创建者：      杨程
// 创建日期：	    2022/12/20 17:42:57

//----------------------------------------------------------------*/

#endregion

using Vampirewal.Admin.ShareModel.Models.Systems.Attachment;

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// 系统RPC接口
/// </summary>
public partial class SystemService : RpcServiceBasic
{
    /// <summary>
    ///
    /// </summary>
    public SystemService()
    {
        //构造函数
    }

    #region [     验证租户     ]

    /// <summary>
    /// 验证租户是否能用
    /// </summary>
    /// <param name="TenantId"></param>
    /// <returns></returns>
    [TouchRpc]
    public bool CheckTenant(string TenantId)
    {
        var tenant = SqlSugarRepository<Sys_Tenant>().Single(s => s.BillId == TenantId);

        if (tenant != null && tenant.IsActive)
        {
            if (tenant.EndTime >= DateTime.Now)
            {
                return true;
            }
        }

        return false;
    }

    #endregion

    #region [     权限     ]

    /// <summary>
    /// 获取当前用户的页面权限
    /// </summary>
    /// <param name="callContext"></param>
    /// <returns></returns>
    [Description("获取当前用户的权限")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    [ActionFilter]
    public List<Sys_RolePermission> GetPermissions(ICallContext callContext)
    {
        callContext.CheckCaller(out Sys_User user);

        var userrole = SqlSugarRepository<Sys_UserRole>().Single(s => s.UserId == user.BillId);

        var permissions = SqlSugarRepository<Sys_RolePermission>().ToList(s => s.RoleId == userrole.RoleId);

        return permissions;
    }

    #endregion

    #region [     菜单     ]

    /// <summary>
    /// 获取当前用户的菜单
    /// </summary>
    /// <param name="callContext"></param>
    /// <returns></returns>
    [Description("获取当前用户的菜单")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    [ActionFilter]
    public List<Sys_Menu> GetLoginUserMenus(ICallContext callContext)
    {
        callContext.CheckCaller(out Sys_User user);

        var userrole = SqlSugarRepository<Sys_UserRole>().Single(s => s.UserId == user.BillId);
        var permissions = SqlSugarRepository<Sys_RolePermission>().ToList(s => s.RoleId == userrole.RoleId);
        var CurMenus = SqlSugarRepository<Sys_Menu>().ToList(t => permissions.Select(s => s.PermissionId).Contains(t.BillId));

        return CurMenus;
    }

    #endregion

    #region [     系统升级     ]

    /// <summary>
    /// 获取当前客户端最新版本
    /// </summary>
    /// <returns>系统版本</returns>
    [Description("获取当前客户端最新版本")]
    [TouchRpc]
    [ActionFilter]
    public Sys_AppVersion GetLastVersion()
    {
        var last = SqlSugarRepository<Sys_AppVersion>().Context.Queryable<Sys_AppVersion>().OrderByDescending(o => o.PublishTime).First();

        return last;
    }

    /// <summary>
    /// 新增客户端版本
    /// </summary>
    /// <param name="version"></param>
    /// <returns></returns>
    [Description("新增客户端版本")]
    [TouchRpc]
    [ActionFilter]
    public Sys_AppVersion CreateNewVersion(Sys_AppVersion version)
    {
        var repAppversion = SqlSugarRepository<Sys_AppVersion>();

        if (!repAppversion.Any(a => a.Version == version.Version))
        {
            try
            {
                repAppversion.CurrentBeginTran();

                var success = repAppversion.InsertReturnEntity(version);

                repAppversion.CurrentCommitTran();

                return success ;
            }
            catch (Exception ex)
            {
                repAppversion.CurrentRollbackTran();
                throw ex;
            }
        }

        throw new Exception("重复版本号!");
    }

    /// <summary>
    /// 获取附件信息
    /// </summary>
    /// <param name="Billid"></param>
    /// <returns></returns>
    [Description("获取附件信息")]
    [TouchRpc]
    [ActionFilter]
    public Sys_Attachment GetAttachmentById(string Billid)
    {
        return SqlSugarRepository<Sys_Attachment>().Single(s => s.BillId == Billid);
    }

    [Description("获取附件信息")]
    [TouchRpc]
    [ActionFilter]
    public bool AddNewAttachment(Sys_Attachment attachment)
    {
        var repAtt= SqlSugarRepository<Sys_Attachment>();

        try
        {
            repAtt.CurrentBeginTran();
            int num= repAtt.Insert(attachment);
            repAtt.CurrentCommitTran();

            return num == 1;
        }
        catch (Exception ex)
        {
            repAtt.CurrentRollbackTran();
            throw ex;
        }
    }
    #endregion
}