﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    FlowEngineService
// 创建者：      杨程
// 创建日期：	    2023/1/17 13:39:22

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// 流程引擎RPC服务
/// </summary>
public partial class FlowEngineService : RpcServiceBasic
{
    /// <summary>
    ///
    /// </summary>
    public FlowEngineService()
    {
        //构造函数
    }

    #region [     查询     ]

    /// <summary>
    /// 查询当前租户下的所有流程实例
    /// </summary>
    /// <param name="callContext">上下文</param>
    /// <param >上下文</param>
    /// <returns></returns>
    [Description("查询当前租户下的所有流程实例")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public List<FlowBaseInfo> GetFlowList(ICallContext callContext)
    {
        callContext.CheckCaller(out Sys_User user);

        return SqlSugarRepository<FlowBaseInfo>().ToList(s => s.TenantId == user.TenantId);
    }

    /// <summary>
    /// 查询当前租户单个流程实例
    /// </summary>
    /// <param name="callContext">上下文</param>
    /// <param name="BillId">流程ID</param>
    /// <returns></returns>
    [Description("查询当前租户单个流程实例")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public FlowBaseInfo GetFlowBaseInfo(ICallContext callContext, string BillId)
    {
        callContext.CheckCaller(out Sys_User user);

        return SqlSugarRepository<FlowBaseInfo>().Single(s => s.TenantId == user.TenantId && s.BillId == BillId);
    }

    /// <summary>
    /// 通过流程ID查询流程明细
    /// </summary>
    /// <param name="BillId">流程ID</param>
    /// <returns></returns>
    [Description("通过流程ID查询流程明细")]
    [ActionFilter]
    [TouchRpc]
    public List<FlowStep> GetFlowStepById(string BillId)
    {
        return SqlSugarRepository<FlowStep>().ToList(s => s.BillId == BillId);
    }

    #endregion

    #region [     新增     ]

    /// <summary>
    /// 新增流程基本信息
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="info"></param>
    [LogWrite]
    [Description("新增流程基本信息")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public void CreateFlowBaseInfo(ICallContext callContext, FlowBaseInfo info)
    {
        var repFlowBaseInfo = SqlSugarRepository<FlowBaseInfo>();
        try
        {
            repFlowBaseInfo.CurrentBeginTran();

            repFlowBaseInfo.Insert(info);

            repFlowBaseInfo.CurrentCommitTran();
        }
        catch (Exception ex)
        {
            repFlowBaseInfo.CurrentRollbackTran();
            throw ex;
        }
    }

    /// <summary>
    /// 新增流程步骤信息
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="steps"></param>
    [LogWrite]
    [Description("新增流程步骤信息")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public void CreateFlowStep(ICallContext callContext, List<FlowStep> steps)
    {
        var repFlowStep = SqlSugarRepository<FlowStep>();
        try
        {
            repFlowStep.CurrentBeginTran();

            repFlowStep.Insert(steps);

            repFlowStep.CurrentCommitTran();
        }
        catch (Exception ex)
        {
            repFlowStep.CurrentRollbackTran();
            throw ex;
        }
    }

    #endregion

    #region [     修改     ]

    /// <summary>
    /// 修改流程基本信息
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="info"></param>
    [LogWrite]
    [Description("修改流程基本信息")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public void UpdateFlowBaseInfo(ICallContext callContext, FlowBaseInfo info)
    {
        var repFlowBaseInfo = SqlSugarRepository<FlowBaseInfo>();
        try
        {
            repFlowBaseInfo.CurrentBeginTran();

            repFlowBaseInfo.Update(info);

            repFlowBaseInfo.CurrentCommitTran();
        }
        catch (Exception ex)
        {
            repFlowBaseInfo.CurrentRollbackTran();
            throw ex;
        }
    }

    [LogWrite]
    [Description("修改流程步骤信息")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public void UpdateFlowStep(ICallContext callContext, List<FlowStep> steps)
    {
        var repFlowStep = SqlSugarRepository<FlowStep>();
        try
        {
            repFlowStep.CurrentBeginTran();

            repFlowStep.Update(steps);

            repFlowStep.CurrentCommitTran();
        }
        catch (Exception ex)
        {
            repFlowStep.CurrentRollbackTran();
            throw ex;
        }
    }

    #endregion
}