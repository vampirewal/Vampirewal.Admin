﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    ClientNotifyServicee
// 创建者：      杨程
// 创建日期：	    2023/1/18 12:48:06

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// 客户端通知RPC服务
/// </summary>
public partial class ClientNotifyServicee : RpcServiceBasic
{
    private HttpTouchRpcService HttpService { get; set; }

    /// <summary>
    ///
    /// </summary>
    public ClientNotifyServicee(HttpTouchRpcService httpService)
    {
        HttpService = httpService;

        //构造函数
    }

    /// <summary>
    /// 给其他客户端发送通知
    /// </summary>
    /// <param name="callContext"></param>
    /// <returns></returns>
    [LogWrite]
    [Description("给其他客户端发送通知")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public void SendMessage(ICallContext callContext, Sys_Notify notify)
    {
        callContext.CheckCaller(out Sys_User user);

        #region 判断当前调用用户与接收通知的用户，是否同一个租户内

        var repNotify = SqlSugarRepository<Sys_Notify>();

        var OtherUser = VampirewalCoreContext.GetInstance().GetContext<Sys_User>(notify.ReceiveUserId);

        if (OtherUser == null)
        {
            repNotify.Insert(notify);
            return;
        }

        if (OtherUser != null && user.TenantId != OtherUser.TenantId)
        {
            return;
        }

        #endregion

        var httpclient = VampirewalCoreContext.GetInstance().GetContext<HttpTouchRpcSocketClient>($"{notify.ReceiveUserId}-httpclient");

        if (httpclient != null)
        {
            try
            {
                httpclient.Invoke("Vampirewal.Admin.Client.ClientRpcServer.ReverseCallbackServer.GetNotifyMessage".ToLower(), InvokeOption.OnlySend, notify.NotifyMsg);
                notify.IsReceived = true;
                notify.ReceivedTime = DateTime.Now;
                repNotify.Insert(notify);
            }
            catch
            {
                repNotify.Insert(notify);
            }
        }
    }
}