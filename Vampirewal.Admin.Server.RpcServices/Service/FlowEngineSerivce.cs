﻿#region [     文件信息     ]
/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.Server.RpcServices.Service
 * 唯一标识：f3e84701-0abd-4e1b-a86a-1e6491a0532e
 * 文件名：FlowEngineSerivce
 * 当前用户域：VAMPIREWAL
 * 
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/13 14:00:26
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion [     文件信息     ]




using Vampirewal.Core.Interface;

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// 流程引擎RPC服务
/// </summary>
public partial class FlowEngineSerivce : RpcServiceBasic
{
    private IVampirewalCoreValidationService ValidationService { get; set; }

    public FlowEngineSerivce(IVampirewalCoreValidationService validationService)
    {
        ValidationService = validationService;
        //构造函数
    }

    #region [     新增     ]

    /// <summary>
    /// 新增流程设计
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="baseInfo"></param>
    /// <param name="steps"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [Description("新增流程设计")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public bool FlowDesignCreate(ICallContext callContext, FlowBaseInfo baseInfo, List<FlowStep> steps)
    {
        callContext.CheckCaller(out Sys_User user);

        if (!user.IsAdmin)
        {
            return false;
        }

        #region 验证是否重复
        var repBaseInfo = SqlSugarRepository<FlowBaseInfo>();
        if (repBaseInfo.IsExists(a => a.TenantId == user.TenantId && a.OrgId == baseInfo.OrgId && a.BillType == baseInfo.BillType && a.State == 1))
        {
            throw new Exception("库中已有该机构/部门的流程设置");
        }
        #endregion

        if (!ValidationService.IsValidation(baseInfo))
        {
            //ValidationService.ErrorList
            throw new Exception($"{string.Join("\r\n", ValidationService.ErrorList)}");

        }

        try
        {
            repBaseInfo.BeginTran();

            repBaseInfo.Insert(baseInfo);
            repBaseInfo.Change<FlowStep>().Insert(steps);

            repBaseInfo.CommitTran();
            return true;
        }
        catch (Exception ex)
        {
            repBaseInfo.RollbackTran();
            throw ex;
        }



    }

    #endregion

    #region [     公开方法     ]

    #endregion

    #region [     私有方法     ]

    #endregion

    #region [     Command命令     ]

    #endregion

}
