﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    DicService
// 创建者：      杨程
// 创建日期：	    2022/12/16 10:17:18

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// 字典RPC服务
/// </summary>
public partial class DicService : RpcServiceBasic
{
    /// <summary>
    ///
    /// </summary>
    public DicService()
    {
        //构造函数
    }

    #region [     CURD     ]

    #region 新增

    /// <summary>
    /// 新增字典类型
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [LogWrite]
    [ActionFilter]
    [Description("新增字典类型")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public Sys_DicCategory InsertCategory(ICallContext callContext, Sys_DicCategory model)
    {
        callContext.CheckCaller(out Sys_User user);

        var repDicCategory = SqlSugarRepository<Sys_DicCategory>();

        try
        {
            if (repDicCategory.Any(a => a.DicCategoryName.Equals(model.DicCategoryName) && a.TenantId == user.TenantId))
            {
                throw new Exception("已存在相同类型的字典");
            }

            repDicCategory.CurrentBeginTran();

            var returnEntity = repDicCategory.InsertReturnEntity(model);

            repDicCategory.CurrentCommitTran();

            return returnEntity;
        }
        catch (Exception ex)
        {
            repDicCategory.CurrentRollbackTran();
            throw new Exception(ex.Message);
        }
    }

    /// <summary>
    /// 新增字典值
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    [ActionFilter]
    [Description("新增字典值")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public bool InsertDicValue(ICallContext callContext, List<Sys_Dic> model)
    {
        callContext.CheckCaller(out Sys_User user);
        var repDic = SqlSugarRepository<Sys_Dic>();
        try
        {
            repDic.CurrentBeginTran();

            repDic.Insert(model);

            repDic.CurrentCommitTran();

            return true;
        }
        catch (Exception ex)
        {
            repDic.CurrentRollbackTran();
            return false;
        }
    }

    #endregion

    #region 删除

    /// <summary>
    /// 删除字典类型
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    [LogWrite]
    [Description("删除字典类型")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public bool DeleteDicCategory(ICallContext callContext, string model)
    {
        callContext.CheckCaller(out Sys_User user);
        var entity = JsonConvert.DeserializeObject<Sys_DicCategory>(model);

        var repDicCategory = SqlSugarRepository<Sys_DicCategory>();
        var repDic = SqlSugarRepository<Sys_Dic>();

        var dics = repDic.ToList(s => s.BillId == entity.BillId);

        try
        {
            repDicCategory.BeginTran();

            repDic.Delete(dics.Select(s => s.DtlId));

            repDicCategory.Delete(entity);

            repDicCategory.CommitTran();

            return true;
        }
        catch (Exception ex)
        {
            repDicCategory.RollbackTran();
            return false;
        }
    }

    #endregion

    #region 查

    /// <summary>
    /// 查询所有字典类型
    /// </summary>
    /// <param name="callContext"></param>
    /// <returns></returns>
    [LogWrite]
    [Description("查询所有字典类型")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public List<Sys_DicCategory> GetAllDicCategory(ICallContext callContext)
    {
        callContext.CheckCaller(out Sys_User user);
        var dbresult = SqlSugarRepository<Sys_DicCategory>().ToList(f => f.IsPublic || f.TenantId == user.TenantId);

        return dbresult;
    }

    /// <summary>
    /// 分页查询字典类型
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    [LogWrite]
    [Description("分页查询字典类型")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public PageContainer<Sys_DicCategory> GetPageListDicCategory(ICallContext callContext, PageRequest request)
    {
        int total = 0;
        int totalPage = 0;

        callContext.CheckCaller(out Sys_User user);
        var dbresult = SqlSugarRepository<Sys_DicCategory>().AsQueryable().Where(f => f.IsPublic || f.TenantId == user.TenantId)
                                                                         .ToPageList(request.Page, request.Limit, ref total, ref totalPage);

        PageContainer<Sys_DicCategory> container = new PageContainer<Sys_DicCategory>();
        container.Data = dbresult;
        container.TotalCount = total;
        container.PageCount = totalPage;
        container.Count = dbresult.Count;

        return container;
    }

    /// <summary>
    /// 通过字典类型BillId查询字典值
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="BillId"></param>
    /// <returns></returns>
    [Description("通过字典类型BillId查询字典值")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public List<Sys_Dic> GetDicValueByCategory(ICallContext callContext, string BillId)
    {
        callContext.CheckCaller(out Sys_User user);

        var repDicCategory = SqlSugarRepository<Sys_DicCategory>();
        var repDic = SqlSugarRepository<Sys_Dic>();

        repDic.ToList(s => s.BillId == BillId);
        //todo 此处还差一个判断，如果是IsPublic的那种字典
        var dbresult = repDicCategory.AsQueryable()
                                                 .InnerJoin<Sys_Dic>((d1, d2) => d1.BillId == d2.BillId)
                                                 .Where((d1, d2) => (d1.TenantId == user.TenantId || d1.IsPublic) && d2.BillId == BillId)
                                                 .Select<Sys_Dic>()
                                                 .ToList();

        return dbresult;
    }

    /// <summary>
    /// 通过字典类型Id查询字典值并转换成客户端使用的枚举值
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="DicCategoryId"></param>
    /// <returns></returns>
    [Description("通过字典类型Id查询字典值并转换成客户端使用的枚举值")]
    [ActionFilter]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public List<EnumberCreditType> GetDicEnumberCreditListByCategoryId(ICallContext callContext, string DicCategoryId)
    {
        callContext.CheckCaller(out Sys_User user);
        var result = new List<EnumberCreditType>();

        GetDicValueByCategory(callContext, DicCategoryId).ForEach(f =>
        {
            EnumberCreditType enumber = new EnumberCreditType()
            {
                Desction = f.Description,
                Value = f.DicValue,
                Key = f.Description,
                Classification = f.DicValue,
                Name = f.Description,
            };

            result.Add(enumber);
        });

        return result;
    }

    #endregion

    #endregion
}