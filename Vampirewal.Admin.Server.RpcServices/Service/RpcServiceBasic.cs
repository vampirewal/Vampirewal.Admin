﻿#region [     文件信息     ]

/*----------------------------------------------------------------
 * CLR版本：4.0.30319.42000
 * 机器名称：VAMPIREWAL
 * 公司名称：Organization
 * 命名空间：Vampirewal.Admin.Server.RpcServices.Service
 * 唯一标识：e57bfc53-5e9c-408d-a775-85189155a738
 * 文件名：RpcServiceBasic
 * 当前用户域：VAMPIREWAL
 *
 * 创建者：Administrator
 * 电子邮箱：1425271996@qq.com
 * 创建时间：2023/3/1 14:03:36
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/

#endregion [     文件信息     ]

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// 单例RPC服务基类
/// </summary>
public partial class RpcServiceBasic : RpcServer
{
    /// <summary>
    /// 仓储数据库
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    protected SqlSugarRepository<T> SqlSugarRepository<T>() where T : class,new()
    {
        return VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<T>>();
    }
}

/// <summary>
/// 瞬时RPC服务基类
/// </summary>
public partial class TransientRpcServiceBasic : TransientRpcServer
{
    /// <summary>
    /// 仓储数据库
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    protected SqlSugarRepository<T> SqlSugarRepository<T>() where T : class, new()
    {
        return VampirewalCoreContext.GetInstance().GetService<SqlSugarRepository<T>>();
    }
}

