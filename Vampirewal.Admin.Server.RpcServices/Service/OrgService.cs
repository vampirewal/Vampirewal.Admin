﻿#region 文件信息

/*----------------------------------------------------------------
//
// 文件名称：
// 文件功能描述：
// 设计要求：
//
// 文 件 名：    OrgService
// 创建者：      杨程
// 创建日期：	    2023/1/30 13:13:55

//----------------------------------------------------------------*/

#endregion

namespace Vampirewal.Admin.Server.RpcServices.Service;

/// <summary>
/// 机构RPC服务
/// </summary>
public partial class OrgService : RpcServiceBasic
{

    /// <summary>
    ///
    /// </summary>
    public OrgService()
    {
        //构造函数
    }

    #region [     查询     ]

    /// <summary>
    /// 通过Id查询机构
    /// </summary>
    /// <param name="callContext"></param>
    /// <param name="BillId"></param>
    /// <returns></returns>
    [Description("通过Id查询机构")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public Sys_Org GetOrgById(ICallContext callContext, string BillId)
    {
        callContext.CheckCaller(out Sys_User user);
        return SqlSugarRepository<Sys_Org>().FirstOrDefault(f => f.BillId == BillId && f.TenantId == user.TenantId);
    }

    /// <summary>
    /// 查询当前租户下所有的机构
    /// </summary>
    /// <param name="callContext"></param>
    /// <returns></returns>
    [Description("查询当前租户下所有的机构")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public List<Sys_Org> GetAllOrg(ICallContext callContext)
    {
        callContext.CheckCaller(out Sys_User user);

        if (!user.IsAdmin)
        {
            throw new Exception("该接口仅管理员可使用");
        }

        return SqlSugarRepository<Sys_Org>().ToList(t => t.TenantId == user.TenantId);
    }

    /// <summary>
    /// 查询当前机构的下级机构
    /// </summary>
    /// <param name="callContext"></param>
    /// <returns></returns>
    [Description("查询当前机构的下级机构")]
    [TouchRpc(MethodFlags = MethodFlags.IncludeCallContext)]
    public List<Sys_Org> GetSubordinateOrg(ICallContext callContext)
    {
        callContext.CheckCaller(out Sys_User user);

        List<Sys_Org> subordinateOrgList = new List<Sys_Org>();

        FindSubordinateOrg(SqlSugarRepository<Sys_Org>(),user.OrgId, ref subordinateOrgList);

        return subordinateOrgList;
    }

    /// <summary>
    /// 查找下级机构
    /// </summary>
    /// <param name="ParentOrgId"></param>
    /// <param name="list"></param>
    private void FindSubordinateOrg(SqlSugarRepository<Sys_Org> repOrg, string ParentOrgId, ref List<Sys_Org> list)
    {
        var Childs = repOrg.ToList(t => t.ParentId == ParentOrgId);

        foreach (var org in Childs)
        {
            FindSubordinateOrg(repOrg,org.BillId, ref list);

            list.Add(org);
        }
    }

    #endregion
}